# initialize db for financium

# create view
CREATE VIEW vw_dashboard AS
    SELECT
		t.id,
        t.name as 'transaction_name',
        t.category,
        t.transaction_date,
        t.amount,
        t.currency,
        tt.name as 'transaction_type',
        t.user_id
    FROM
        transaction t inner join transaction_type tt on t.transaction_type_id = tt.id;
        
# create indexes
alter table transaction
add index idx_wallet_id_transaction_date(wallet_id, transaction_date);
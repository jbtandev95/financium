package com.jbtan.financium.projection;

import java.math.BigDecimal;

public interface TransactionMonthlySummaryProjection {
    String getMonth();

    String getCategory();

    BigDecimal getAmount();
}

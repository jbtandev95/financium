package com.jbtan.financium.command;

import com.jbtan.financium.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TransactionCommand {

    private final TransactionService transactionService;
}

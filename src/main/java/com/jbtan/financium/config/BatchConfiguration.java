package com.jbtan.financium.config;

import com.jbtan.financium.batch.BatchMigrationChunkListener;
import com.jbtan.financium.batch.BatchMigrationItemReaderListener;
import com.jbtan.financium.batch.SpendeeMigrationItemProcessor;
import com.jbtan.financium.batch.SpendeeMigrationStepThreeTasklet;
import com.jbtan.financium.batch.SpendeeMigrationStepTwoTasklet;
import com.jbtan.financium.domain.SpendeeTransactionMigration;
import com.jbtan.financium.repository.SpendeeTransactionMigrationRepository;
import com.jbtan.financium.service.dto.SpendeeMigrationDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;

@EnableBatchProcessing
@Configuration
@Slf4j
@RequiredArgsConstructor
public class BatchConfiguration {

    private final JobBuilderFactory jobBuilderFactory;

    private final StepBuilderFactory stepBuilderFactory;

    private final SpendeeMigrationItemProcessor spendeeMigrationItemProcessor;

    private final SpendeeTransactionMigrationRepository spendeeTransactionMigrationRepository;

    private final SpendeeMigrationStepTwoTasklet spendeeMigrationStepTwoTasklet;

    private final SpendeeMigrationStepThreeTasklet spendeeMigrationStepThreeTasklet;

    @Value("/Users/jbtan/Downloads/transactions_export_2023-01-22_cash-wallet.csv")
    private PathResource inputResource;

    @Bean
    public Step spendeeMigrationStepOne() {
        return stepBuilderFactory
            .get("migrate_step_one")
            .<SpendeeMigrationDTO, SpendeeTransactionMigration>chunk(10)
            .reader(reader())
            .processor(spendeeMigrationItemProcessor)
            .writer(writer())
            .build();
    }

    @Bean
    public Step spendeeMigrationStepTwo() {
        return stepBuilderFactory.get("migrate_step_two").tasklet(spendeeMigrationStepTwoTasklet).build();
    }

    @Bean
    public Step spendeeMigrationStepThree() {
        return stepBuilderFactory.get("migrate_step_three").tasklet(spendeeMigrationStepThreeTasklet).build();
    }

    @Bean
    public FlatFileItemReader<SpendeeMigrationDTO> reader() {
        FlatFileItemReader<SpendeeMigrationDTO> itemReader = new FlatFileItemReader<SpendeeMigrationDTO>();
        itemReader.setLineMapper(lineMapper());
        itemReader.setLinesToSkip(1);
        itemReader.setResource(inputResource);
        return itemReader;
    }

    @Bean
    public LineMapper<SpendeeMigrationDTO> lineMapper() {
        DefaultLineMapper<SpendeeMigrationDTO> lineMapper = new DefaultLineMapper<SpendeeMigrationDTO>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(new String[] { "date", "wallet", "type", "categoryName", "amount", "currency", "note", "labels", "author" });
        lineTokenizer.setDelimiter(",");

        BeanWrapperFieldSetMapper<SpendeeMigrationDTO> fieldSetMapper = new BeanWrapperFieldSetMapper<SpendeeMigrationDTO>();
        fieldSetMapper.setTargetType(SpendeeMigrationDTO.class);
        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(fieldSetMapper);
        return lineMapper;
    }

    @Bean
    public RepositoryItemWriter<SpendeeTransactionMigration> writer() {
        RepositoryItemWriter<SpendeeTransactionMigration> itemWriter = new RepositoryItemWriter<SpendeeTransactionMigration>();
        itemWriter.setRepository(spendeeTransactionMigrationRepository);
        itemWriter.setMethodName("save");

        return itemWriter;
    }

    @Bean
    public Job migrationVersionOneJob() {
        return jobBuilderFactory
            .get("spendee_migration")
            .listener(new BatchMigrationItemReaderListener())
            .listener(new BatchMigrationChunkListener())
            .flow(spendeeMigrationStepOne())
            .next(spendeeMigrationStepTwo())
            .next(spendeeMigrationStepThree())
            .end()
            .build();
    }
}

package com.jbtan.financium.repository;

import com.jbtan.financium.domain.Wallet;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Wallet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {
    @Query("select wallet from Wallet wallet where wallet.user.login = ?#{principal.username}")
    List<Wallet> findByUserIsCurrentUser();

    @Query("select wl from Wallet wl " + "where wl.user.login = ?#{principal.username} " + "and wl.id = :walletId ")
    Optional<Wallet> findCurrentUserWalletByWalletId(@Param("walletId") Long walletId);

    @Query("select wl.id from Wallet wl " + "where wl.user.login = ?#{principal.username} " + "and wl.id = :walletId ")
    Optional<Long> findIfWalletIdBelongsToCurrentUser(@Param("walletId") Long walletId);
}

package com.jbtan.financium.repository;

import com.jbtan.financium.domain.TransactionTypeMigration;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the TransactionTypeMigration entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransactionTypeMigrationRepository extends JpaRepository<TransactionTypeMigration, Long> {
    @Query(
        "select transactionTypeMigration from TransactionTypeMigration transactionTypeMigration where transactionTypeMigration.user.login = ?#{principal.username}"
    )
    List<TransactionTypeMigration> findByUserIsCurrentUser();
}

package com.jbtan.financium.repository;

import com.jbtan.financium.domain.Transaction;
import com.jbtan.financium.domain.enumeration.TransactionCategory;
import com.jbtan.financium.domain.enumeration.TransactionStatus;
import com.jbtan.financium.projection.TransactionMonthlySummaryProjection;
import com.jbtan.financium.service.dto.DashboardGroupedSummaryDTO;
import com.jbtan.financium.service.dto.DashboardSummaryDTO;
import java.time.ZonedDateTime;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Transaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    @Query("select t from Transaction t where t.user.login = ?#{principal.username}")
    List<Transaction> findByUserIsCurrentUser();

    @Query(
        "select new com.jbtan.financium.service.dto.DashboardSummaryDTO(sum(case when category = 'EXPENSES' then amount else 0 end), sum(case when category = 'INCOME' then amount else 0 end)) " +
        "from Transaction t " +
        "where t.user.login = ?#{principal.username} " +
        "and t.transactionDate >= :start " +
        "and t.transactionDate <= :end " +
        "and t.wallet.id = :walletId "
    )
    DashboardSummaryDTO getCurrentUserSummaryByWalletIdAndDateTime(
        @Param("walletId") Long walletId,
        @Param("start") ZonedDateTime start,
        @Param("end") ZonedDateTime end
    );

    @Query(
        "select new com.jbtan.financium.service.dto.DashboardGroupedSummaryDTO(tt.name, sum(t.amount), count(t.id)) " +
        "from Transaction t " +
        "inner join TransactionType tt on t.transactionType.id = tt.id " +
        "where t.user.login = ?#{principal.username} " +
        "and t.transactionDate >= :start " +
        "and t.transactionDate <= :end " +
        "and t.category = :category " +
        "and t.wallet.id = :walletId " +
        "group by tt.name " +
        "order by sum(t.amount)"
    )
    List<DashboardGroupedSummaryDTO> getCurrentUserGroupedSummariesByWalletIdAndDateTime(
        @Param("walletId") Long walletId,
        @Param("category") TransactionCategory category,
        @Param("start") ZonedDateTime start,
        @Param("end") ZonedDateTime end,
        Pageable pageable
    );

    @Query(
        "select t from Transaction t " +
        "where t.user.login = ?#{principal.username} " +
        "and t.status = :status and t.wallet.id = :walletId and t.transactionDate >= :start and t.transactionDate <= :end "
    )
    Page<Transaction> getCurrentUserTransactionList(
        @Param("walletId") Long walletId,
        @Param("status") TransactionStatus status,
        @Param("start") ZonedDateTime start,
        @Param("end") ZonedDateTime end,
        Pageable pageable
    );

    @Query(
        "select t from Transaction t " +
        "where t.user.login = ?#{principal.username} " +
        "and t.status = :status and t.wallet.id = :walletId "
    )
    Page<Transaction> getCurrentUserLatestTransactionList(
        @Param("walletId") Long walletId,
        @Param("status") TransactionStatus status,
        Pageable pageable
    );

    @Query(
        value = "select DATE_FORMAT(CONVERT_TZ(transaction_date, '+00:00', :timeZoneId), '%Y-%m') AS month, category, sum(amount) as amount " +
        "FROM transaction where wallet_id = :walletId and transaction_date >= :start and transaction_date <= :end " +
        "GROUP BY month, category ORDER BY month ",
        nativeQuery = true
    )
    List<TransactionMonthlySummaryProjection> getMonthlySummaryFromDateRange(
        @Param("walletId") Long walletId,
        @Param("start") ZonedDateTime start,
        @Param("end") ZonedDateTime end,
        @Param("timeZoneId") String timeZoneId
    );
}

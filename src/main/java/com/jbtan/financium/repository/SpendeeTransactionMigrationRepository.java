package com.jbtan.financium.repository;

import com.jbtan.financium.domain.SpendeeTransactionMigration;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the SpendeeTransactionMigration entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpendeeTransactionMigrationRepository extends JpaRepository<SpendeeTransactionMigration, Long> {
    @Query("select stm from SpendeeTransactionMigration stm where stm.user.login = ?#{principal.username}")
    List<SpendeeTransactionMigration> findByUserIsCurrentUser();

    @Query("select distinct(categoryName) from SpendeeTransactionMigration where user.id = :userId ")
    List<String> findDistinctCategoryByUserId(@Param("userId") Long userId);

    @Query("select stm from SpendeeTransactionMigration stm where stm.categoryName = :categoryName ")
    List<SpendeeTransactionMigration> findAllByCategoryName(@Param("categoryName") String categoryName);
}

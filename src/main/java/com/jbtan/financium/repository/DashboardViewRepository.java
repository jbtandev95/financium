package com.jbtan.financium.repository;

import com.jbtan.financium.domain.DashboardView;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DashboardViewRepository extends ReadOnlyRepository<DashboardView, Long> {
    @Query("select vw from DashboardView vw where vw.userId = :userId ")
    List<DashboardView> findByUserId(@Param("userId") Long userId);
}

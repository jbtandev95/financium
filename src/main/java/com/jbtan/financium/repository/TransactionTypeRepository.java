package com.jbtan.financium.repository;

import com.jbtan.financium.domain.TransactionType;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the TransactionType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransactionTypeRepository extends JpaRepository<TransactionType, Long> {
    @Query("select t from TransactionType t where t.user.login = ?#{principal.username}")
    List<TransactionType> findByUserIsCurrentUser();

    @Query("select t from TransactionType t where t.user.id = :userId ")
    List<TransactionType> findByTransactionTypeListByUserId(@Param("userId") Long userId);
}

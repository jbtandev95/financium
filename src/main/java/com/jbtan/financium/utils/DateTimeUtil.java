package com.jbtan.financium.utils;

import com.jbtan.financium.domain.enumeration.CurrencyCode;
import com.jbtan.financium.domain.enumeration.DashboardDurationType;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;

public class DateTimeUtil {

    private DateTimeUtil() {}

    public static ZonedDateTime getZonedStartTime(DashboardDurationType type, CurrencyCode currency, Instant now) {
        ZonedDateTime nowZoned = now.atZone(currency.getZoneId());

        if (DashboardDurationType.MONTHLY.equals(type)) {
            return nowZoned.with(TemporalAdjusters.firstDayOfMonth());
        } else if (DashboardDurationType.YEARLY.equals(type)) {
            return nowZoned.with(TemporalAdjusters.firstDayOfYear());
        } else {
            return nowZoned;
        }
    }

    public static ZonedDateTime getZonedStartTime(LocalDate start, CurrencyCode currency) {
        return start.atStartOfDay(currency.getZoneId());
    }

    public static ZonedDateTime getZonedEndTime(LocalDate end, CurrencyCode currency) {
        return end.atStartOfDay(currency.getZoneId()).minusSeconds(1L);
    }
}

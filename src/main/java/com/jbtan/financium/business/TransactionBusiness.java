package com.jbtan.financium.business;

import com.jbtan.financium.service.dto.TransactionDTO;

public interface TransactionBusiness {
    TransactionDTO saveTransaction(TransactionDTO transactionDTO);
}

package com.jbtan.financium.business;

import com.jbtan.financium.domain.enumeration.MigrationSource;

public interface JobBusiness {
    void migrateDataFromOthers(MigrationSource source, Long userId);
}

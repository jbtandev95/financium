package com.jbtan.financium.business.impl;

import com.jbtan.financium.business.UserBusiness;
import com.jbtan.financium.domain.User;
import com.jbtan.financium.domain.enumeration.CurrencyCode;
import com.jbtan.financium.service.MailService;
import com.jbtan.financium.service.UserService;
import com.jbtan.financium.service.WalletService;
import com.jbtan.financium.service.dto.AdminUserDTO;
import com.jbtan.financium.service.dto.WalletDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserBusinessImpl implements UserBusiness {

    private final UserService userService;

    private final MailService mailService;

    private final WalletService walletService;

    @Override
    public User register(AdminUserDTO userDTO, String password, String walletName, CurrencyCode currency) {
        // register user
        User user = userService.registerUser(userDTO, password);

        // create new wallet
        WalletDTO wallet = walletService.register(walletName, currency);

        // send activation email
        mailService.sendActivationEmail(user);

        return user;
    }
}

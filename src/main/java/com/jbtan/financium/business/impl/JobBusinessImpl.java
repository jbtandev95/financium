package com.jbtan.financium.business.impl;

import com.jbtan.financium.business.JobBusiness;
import com.jbtan.financium.domain.enumeration.MigrationSource;
import java.util.Date;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class JobBusinessImpl implements JobBusiness {

    private final JobLauncher jobLauncher;

    private final Job job;

    @Override
    @Async
    public void migrateDataFromOthers(MigrationSource source, Long userId) {
        if (source == MigrationSource.SPENDEE) {
            JobParameters jobParams = new JobParametersBuilder().addLong("userId", userId).addDate("date", new Date()).toJobParameters();

            log.info("running for userId {}", userId);

            try {
                jobLauncher.run(job, jobParams);
            } catch (Exception ex) {
                log.error("Error in migration {}", ex.getMessage());
            }
        }
    }
}

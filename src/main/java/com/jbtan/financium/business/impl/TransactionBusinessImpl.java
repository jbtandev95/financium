package com.jbtan.financium.business.impl;

import com.jbtan.financium.business.TransactionBusiness;
import com.jbtan.financium.domain.enumeration.TransactionStatus;
import com.jbtan.financium.security.SecurityUtils;
import com.jbtan.financium.service.TransactionService;
import com.jbtan.financium.service.UserService;
import com.jbtan.financium.service.WalletService;
import com.jbtan.financium.service.dto.TransactionDTO;
import com.jbtan.financium.service.dto.WalletDTO;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class TransactionBusinessImpl implements TransactionBusiness {

    private final TransactionService transactionService;

    private final UserService userService;

    private final WalletService walletService;

    @Override
    public TransactionDTO saveTransaction(TransactionDTO transactionDTO) {
        String login = SecurityUtils.getCurrentUserLogin().get();

        Long userId = userService.getUserId(login);

        Optional<WalletDTO> wlOpt = walletService.findCurrentUserWalletByWalletId(transactionDTO.getWalletId());

        if (wlOpt.isEmpty()) {
            return null;
        }

        transactionDTO.setStatus(TransactionStatus.A);
        transactionDTO.setUserId(userId);
        transactionDTO.setCurrency(wlOpt.get().getCurrency());

        return transactionService.save(transactionDTO);
    }
}

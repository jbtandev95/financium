package com.jbtan.financium.business.impl;

import com.jbtan.financium.business.SpendeeTransactionBusiness;
import com.jbtan.financium.config.Constants;
import com.jbtan.financium.domain.SpendeeTransactionMigration;
import com.jbtan.financium.domain.enumeration.TransactionCategory;
import com.jbtan.financium.service.TransactionService;
import com.jbtan.financium.service.dto.TransactionDTO;
import com.jbtan.financium.service.mapper.SpendeeTransactionMigrationMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class SpendeeTransactionBusinessImpl implements SpendeeTransactionBusiness {

    private final TransactionService transactionService;

    private final SpendeeTransactionMigrationMapper spendeeTransactionMigrationMapper;

    @Override
    public void save(Long userId, Long transactionTypeId, SpendeeTransactionMigration spendeeRecord) {
        TransactionDTO trnDTO = spendeeTransactionMigrationMapper.toTransactionDTO(spendeeRecord);

        log.info("trnDTO");
        trnDTO.setTransactionTypeId(transactionTypeId);
        trnDTO.setUserId(userId);
        trnDTO.setWalletId(1L);
        trnDTO.setCategory(
            Constants.SPENDEE_EXPENSES.equalsIgnoreCase(spendeeRecord.getType()) ? TransactionCategory.EXPENSES : TransactionCategory.INCOME
        );

        transactionService.save(trnDTO);
    }
}

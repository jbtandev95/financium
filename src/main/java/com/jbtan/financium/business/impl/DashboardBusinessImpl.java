package com.jbtan.financium.business.impl;

import com.jbtan.financium.business.DashboardBusiness;
import com.jbtan.financium.domain.enumeration.CurrencyCode;
import com.jbtan.financium.domain.enumeration.TransactionCategory;
import com.jbtan.financium.domain.enumeration.TransactionStatus;
import com.jbtan.financium.projection.TransactionMonthlySummaryProjection;
import com.jbtan.financium.service.TransactionService;
import com.jbtan.financium.service.WalletService;
import com.jbtan.financium.service.dto.DashboardGroupedSummaryDTO;
import com.jbtan.financium.service.dto.DashboardSummaryDTO;
import com.jbtan.financium.service.dto.TransactionDTO;
import com.jbtan.financium.service.dto.WalletDTO;
import com.jbtan.financium.utils.DateTimeUtil;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service("service")
@RequiredArgsConstructor
@Slf4j
public class DashboardBusinessImpl implements DashboardBusiness {

    private final TransactionService transactionService;

    private final WalletService walletService;

    private final Pageable noPageable = PageRequest.of(0, 500);

    @Override
    public DashboardSummaryDTO getSummary(Long walletId, LocalDate start, LocalDate end) {
        // check if wallet exists
        Optional<WalletDTO> wlOpt = walletService.findOne(walletId);

        WalletDTO wl = wlOpt.get();
        CurrencyCode currency = wl.getCurrency();

        ZonedDateTime zonedStart = DateTimeUtil.getZonedStartTime(start, currency);
        ZonedDateTime zonedEnd = DateTimeUtil.getZonedEndTime(end, currency);

        // get summary total income / expenses by period
        DashboardSummaryDTO dashboardSummary = transactionService.getCurrentUserSummaryByWalletIdAndDateTime(
            wl.getId(),
            zonedStart,
            zonedEnd
        );

        // get sumamry grouped by transaction types by period
        List<DashboardGroupedSummaryDTO> incomeGroupedSummaries = transactionService.getCurrentUserGroupedSummaryByWalletIdAndDateTime(
            wl.getId(),
            TransactionCategory.INCOME,
            zonedStart,
            zonedEnd,
            noPageable
        );

        List<DashboardGroupedSummaryDTO> expensesGroupedSummaries = transactionService.getCurrentUserGroupedSummaryByWalletIdAndDateTime(
            wl.getId(),
            TransactionCategory.EXPENSES,
            zonedStart,
            zonedEnd,
            noPageable
        );

        dashboardSummary.setIncomeGroupedSummaries(incomeGroupedSummaries);
        dashboardSummary.setExpensesGroupedSummaries(expensesGroupedSummaries);

        return dashboardSummary;
    }

    @Override
    public Page<TransactionDTO> getCurrentUserTransactionList(Long walletId, LocalDate start, LocalDate end, Pageable pageable) {
        // check if wallet exists
        Optional<WalletDTO> wlOpt = walletService.findOne(walletId);

        WalletDTO wl = wlOpt.get();
        CurrencyCode currency = wl.getCurrency();

        ZonedDateTime zonedStart = DateTimeUtil.getZonedStartTime(start, currency);
        ZonedDateTime zonedEnd = DateTimeUtil.getZonedEndTime(end, currency);

        return transactionService.getCurrentUserTransactionList(walletId, TransactionStatus.A, zonedStart, zonedEnd, pageable);
    }

    @Override
    public Page<TransactionDTO> getCurrentUserLastTransactionList(Long walletId, int size) {
        return transactionService.getCurrentUserLatestTransactionList(walletId, TransactionStatus.A, size);
    }

    @Override
    public List<DashboardGroupedSummaryDTO> getTopFourSpendings(Long walletId, LocalDate start, LocalDate end) {
        // check if wallet exists
        Optional<WalletDTO> wlOpt = walletService.findOne(walletId);

        WalletDTO wl = wlOpt.get();
        CurrencyCode currency = wl.getCurrency();

        ZonedDateTime zonedStart = DateTimeUtil.getZonedStartTime(start, currency);
        ZonedDateTime zonedEnd = DateTimeUtil.getZonedEndTime(end, currency);

        return transactionService.getCurrentUserGroupedSummaryByWalletIdAndDateTime(
            walletId,
            TransactionCategory.EXPENSES,
            zonedStart,
            zonedEnd,
            PageRequest.of(0, 4)
        );
    }

    @Override
    public Map<String, List<TransactionMonthlySummaryProjection>> getMonthlySummaryFromDateRange(
        Long walletId,
        LocalDate start,
        LocalDate end
    ) {
        // check if wallet exists
        Optional<WalletDTO> wlOpt = walletService.findOne(walletId);

        WalletDTO wl = wlOpt.get();
        CurrencyCode currency = wl.getCurrency();

        ZonedDateTime zonedStart = DateTimeUtil.getZonedStartTime(start, currency);
        ZonedDateTime zonedEnd = DateTimeUtil.getZonedEndTime(end, currency);

        return transactionService
            .getMonthlySummaryFromDateRange(wl, zonedStart, zonedEnd)
            .stream()
            .collect(
                Collectors.groupingBy(
                    TransactionMonthlySummaryProjection::getMonth,
                    TreeMap::new,
                    Collectors.mapping(t -> t, Collectors.toList())
                )
            );
    }
}

package com.jbtan.financium.business;

import com.jbtan.financium.domain.SpendeeTransactionMigration;

public interface SpendeeTransactionBusiness {
    void save(Long userId, Long transactionTypeId, SpendeeTransactionMigration spendeeRecord);
}

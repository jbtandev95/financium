package com.jbtan.financium.business;

import com.jbtan.financium.domain.User;
import com.jbtan.financium.domain.enumeration.CurrencyCode;
import com.jbtan.financium.service.dto.AdminUserDTO;

public interface UserBusiness {
    public User register(AdminUserDTO userDTO, String password, String walletName, CurrencyCode currency);
}

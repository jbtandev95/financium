package com.jbtan.financium.business;

import com.jbtan.financium.projection.TransactionMonthlySummaryProjection;
import com.jbtan.financium.service.dto.DashboardGroupedSummaryDTO;
import com.jbtan.financium.service.dto.DashboardSummaryDTO;
import com.jbtan.financium.service.dto.TransactionDTO;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DashboardBusiness {
    DashboardSummaryDTO getSummary(Long walletId, LocalDate start, LocalDate end);

    Page<TransactionDTO> getCurrentUserTransactionList(Long walletId, LocalDate start, LocalDate end, Pageable pageable);

    Page<TransactionDTO> getCurrentUserLastTransactionList(Long walletId, int size);

    List<DashboardGroupedSummaryDTO> getTopFourSpendings(Long walletId, LocalDate start, LocalDate end);

    Map<String, List<TransactionMonthlySummaryProjection>> getMonthlySummaryFromDateRange(Long walletId, LocalDate start, LocalDate end);
}

package com.jbtan.financium.strategy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.PathResource;
import org.springframework.stereotype.Component;

@Component
public class SpendeeMigrationStrategy implements IMigrationStrategy {

    @Value("/Users/jbtan/Downloads/transactions_export_2023-01-22_cash-wallet.csv")
    private PathResource inputResource;

    @Override
    public void migrate() {
        // step 1
        // get all the transaction types from previous csv file
        // store in a Set
        // batch insert into db with userId

        // step 2
        // trigger batch job run async
        // insert transaction records 1 by 1

    }
}

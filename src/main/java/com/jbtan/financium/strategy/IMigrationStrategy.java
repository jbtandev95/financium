package com.jbtan.financium.strategy;

public interface IMigrationStrategy {
    void migrate();
}

package com.jbtan.financium.decorator;

import com.jbtan.financium.business.DashboardBusiness;
import com.jbtan.financium.projection.TransactionMonthlySummaryProjection;
import com.jbtan.financium.service.WalletNotFoundException;
import com.jbtan.financium.service.WalletService;
import com.jbtan.financium.service.dto.DashboardGroupedSummaryDTO;
import com.jbtan.financium.service.dto.DashboardSummaryDTO;
import com.jbtan.financium.service.dto.TransactionDTO;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component("decorator")
@RequiredArgsConstructor
public class DashboardBusinessDecorator implements DashboardBusiness {

    private final DashboardBusiness dashboardBusiness;

    private final WalletService walletService;

    private void validateCurrentUserWalllet(Long walletId) {
        walletService.findIfWalletIdBelongsToCurrentUser(walletId).orElseThrow(WalletNotFoundException::new);
    }

    @Override
    public DashboardSummaryDTO getSummary(Long walletId, LocalDate start, LocalDate end) {
        validateCurrentUserWalllet(walletId);
        return dashboardBusiness.getSummary(walletId, start, end);
    }

    @Override
    public Page<TransactionDTO> getCurrentUserTransactionList(Long walletId, LocalDate start, LocalDate end, Pageable pageable) {
        validateCurrentUserWalllet(walletId);
        return dashboardBusiness.getCurrentUserTransactionList(walletId, start, end, pageable);
    }

    @Override
    public Page<TransactionDTO> getCurrentUserLastTransactionList(Long walletId, int size) {
        validateCurrentUserWalllet(walletId);
        return dashboardBusiness.getCurrentUserLastTransactionList(walletId, size);
    }

    @Override
    public List<DashboardGroupedSummaryDTO> getTopFourSpendings(Long walletId, LocalDate start, LocalDate end) {
        validateCurrentUserWalllet(walletId);
        return dashboardBusiness.getTopFourSpendings(walletId, start, end);
    }

    @Override
    public Map<String, List<TransactionMonthlySummaryProjection>> getMonthlySummaryFromDateRange(
        Long walletId,
        LocalDate start,
        LocalDate end
    ) {
        validateCurrentUserWalllet(walletId);
        return dashboardBusiness.getMonthlySummaryFromDateRange(walletId, start, end);
    }
}

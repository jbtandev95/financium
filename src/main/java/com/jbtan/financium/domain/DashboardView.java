package com.jbtan.financium.domain;

import com.jbtan.financium.domain.enumeration.CurrencyCode;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "vw_dashboard")
@Immutable
@Getter
public class DashboardView {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "transaction_name")
    private String transactionName;

    @Column(name = "category")
    private String category;

    @Column(name = "transaction_date")
    private ZonedDateTime transactionDate;

    @Column(name = "amount")
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency")
    private CurrencyCode currency;

    @Column(name = "transaction_type")
    private String transactionType;

    @Column(name = "user_id")
    private Long userId;

    @Override
    public String toString() {
        return (
            "DashboardView [id=" +
            id +
            ", transactionName=" +
            transactionName +
            ", category=" +
            category +
            ", transactionDate=" +
            transactionDate +
            ", amount=" +
            amount +
            ", currency=" +
            currency +
            ", transactionType=" +
            transactionType +
            ", userId=" +
            userId +
            "]"
        );
    }
}

package com.jbtan.financium.domain.enumeration;

/**
 * The TransactionCategory enumeration.
 */
public enum TransactionCategory {
    INCOME,
    EXPENSES,
}

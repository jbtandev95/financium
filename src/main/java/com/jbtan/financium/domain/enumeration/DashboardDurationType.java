package com.jbtan.financium.domain.enumeration;

public enum DashboardDurationType {
    MONTHLY,
    YEARLY,
}

package com.jbtan.financium.domain.enumeration;

/**
 * The TransactionStatus enumeration.
 */
public enum TransactionStatus {
    A,
    D,
}

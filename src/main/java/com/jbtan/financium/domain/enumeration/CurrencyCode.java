package com.jbtan.financium.domain.enumeration;

import java.time.ZoneId;
import lombok.Getter;

/**
 * The CurrencyCode enumeration.
 */
@Getter
public enum CurrencyCode {
    MYR(ZoneId.of("Asia/Kuala_Lumpur"), "+08:00"),
    SGD(ZoneId.of("Asia/Singapore"), "+08:00");

    private ZoneId zoneId;
    private String timeZone;

    CurrencyCode(ZoneId zoneId, String timeZone) {
        this.zoneId = zoneId;
        this.timeZone = timeZone;
    }
}

package com.jbtan.financium.batch;

import com.jbtan.financium.service.SpendeeTransactionMigrationService;
import com.jbtan.financium.service.TransactionTypeService;
import com.jbtan.financium.service.dto.TransactionTypeDTO;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class SpendeeMigrationStepTwoTasklet implements Tasklet {

    private final SpendeeTransactionMigrationService spendeeTransactionMigrationService;

    private final TransactionTypeService transactionTypeService;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        Long userId = chunkContext.getStepContext().getStepExecution().getJobParameters().getLong("userId");

        List<String> distinctCategoryName = spendeeTransactionMigrationService.findDistinctCategoryByUserId(userId);

        if (distinctCategoryName.size() == 0) {
            return RepeatStatus.FINISHED;
        }

        List<TransactionTypeDTO> transactionTypeList = distinctCategoryName
            .stream()
            .map(categoryName -> TransactionTypeDTO.builder().name(categoryName).userId(userId).build())
            .collect(Collectors.toList());

        transactionTypeList.stream().forEach(t -> transactionTypeService.save(t));

        return RepeatStatus.FINISHED;
    }
}

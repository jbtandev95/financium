package com.jbtan.financium.batch;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemReadListener;

@Slf4j
public class BatchMigrationItemReaderListener implements ItemReadListener<String> {

    @Override
    public void beforeRead() {
        log.info("[ITEM-READER] BEFORE READ {}");
    }

    @Override
    public void afterRead(String item) {
        log.info("[ITEM-READER] AFTER READ DONE {}", item);
    }

    @Override
    public void onReadError(Exception ex) {
        log.error("[ITEM-READER] FAILED {}", ex.getMessage());
    }
}

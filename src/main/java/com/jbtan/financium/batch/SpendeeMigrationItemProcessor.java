package com.jbtan.financium.batch;

import com.jbtan.financium.domain.SpendeeTransactionMigration;
import com.jbtan.financium.domain.User;
import com.jbtan.financium.domain.enumeration.CurrencyCode;
import com.jbtan.financium.service.dto.SpendeeMigrationDTO;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
@StepScope
public class SpendeeMigrationItemProcessor implements ItemProcessor<SpendeeMigrationDTO, SpendeeTransactionMigration> {

    private final DateTimeFormatter formatter = DateTimeFormatter.ISO_ZONED_DATE_TIME;

    @Value("#{jobParameters['userId']}")
    private Long userId;

    @Override
    public SpendeeTransactionMigration process(SpendeeMigrationDTO item) throws Exception {
        SpendeeTransactionMigration entity = new SpendeeTransactionMigration()
            .amount(item.getAmount())
            .categoryName(item.getCategoryName())
            .currency(CurrencyCode.valueOf(item.getCurrency()))
            .note(item.getNote())
            .transactionDate(ZonedDateTime.parse(item.getDate(), formatter))
            .type(item.getType())
            .user(new User().id(userId));

        return entity;
    }
}

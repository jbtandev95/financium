package com.jbtan.financium.batch;

import com.jbtan.financium.service.dto.SpendeeMigrationDTO;
import java.util.List;
import org.springframework.batch.item.ItemWriter;

public class SpendeeItemWriter implements ItemWriter<SpendeeMigrationDTO> {

    private List<SpendeeMigrationDTO> data;

    @Override
    public void write(List<? extends SpendeeMigrationDTO> items) throws Exception {}
}

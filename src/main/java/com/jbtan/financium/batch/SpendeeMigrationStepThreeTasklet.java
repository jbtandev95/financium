package com.jbtan.financium.batch;

import com.jbtan.financium.business.SpendeeTransactionBusiness;
import com.jbtan.financium.domain.SpendeeTransactionMigration;
import com.jbtan.financium.domain.TransactionType;
import com.jbtan.financium.service.SpendeeTransactionMigrationService;
import com.jbtan.financium.service.TransactionTypeService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class SpendeeMigrationStepThreeTasklet implements Tasklet {

    private final SpendeeTransactionMigrationService spendeeTransactionMigrationService;

    private final TransactionTypeService transactionTypeService;

    private final SpendeeTransactionBusiness spendeeTransactionBusiness;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        Long userId = chunkContext.getStepContext().getStepExecution().getJobParameters().getLong("userId");

        List<TransactionType> transactionTypeList = transactionTypeService.findByTransactionTypeListByUserId(userId);

        transactionTypeList
            .stream()
            .forEach(tt -> {
                List<SpendeeTransactionMigration> migratedData = spendeeTransactionMigrationService.findAllByCategoryName(tt.getName());

                migratedData.forEach(spendeeRecord -> {
                    spendeeTransactionBusiness.save(userId, tt.getId(), spendeeRecord);
                });
            });

        return RepeatStatus.FINISHED;
    }
}

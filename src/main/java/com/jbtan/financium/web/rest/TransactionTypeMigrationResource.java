package com.jbtan.financium.web.rest;

import com.jbtan.financium.repository.TransactionTypeMigrationRepository;
import com.jbtan.financium.service.TransactionTypeMigrationService;
import com.jbtan.financium.service.dto.TransactionTypeMigrationDTO;
import com.jbtan.financium.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jbtan.financium.domain.TransactionTypeMigration}.
 */
@RestController
@RequestMapping("/api")
public class TransactionTypeMigrationResource {

    private final Logger log = LoggerFactory.getLogger(TransactionTypeMigrationResource.class);

    private static final String ENTITY_NAME = "transactionTypeMigration";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TransactionTypeMigrationService transactionTypeMigrationService;

    private final TransactionTypeMigrationRepository transactionTypeMigrationRepository;

    public TransactionTypeMigrationResource(
        TransactionTypeMigrationService transactionTypeMigrationService,
        TransactionTypeMigrationRepository transactionTypeMigrationRepository
    ) {
        this.transactionTypeMigrationService = transactionTypeMigrationService;
        this.transactionTypeMigrationRepository = transactionTypeMigrationRepository;
    }

    /**
     * {@code POST  /transaction-type-migrations} : Create a new transactionTypeMigration.
     *
     * @param transactionTypeMigrationDTO the transactionTypeMigrationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new transactionTypeMigrationDTO, or with status {@code 400 (Bad Request)} if the transactionTypeMigration has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/transaction-type-migrations")
    public ResponseEntity<TransactionTypeMigrationDTO> createTransactionTypeMigration(
        @Valid @RequestBody TransactionTypeMigrationDTO transactionTypeMigrationDTO
    ) throws URISyntaxException {
        log.debug("REST request to save TransactionTypeMigration : {}", transactionTypeMigrationDTO);
        if (transactionTypeMigrationDTO.getId() != null) {
            throw new BadRequestAlertException("A new transactionTypeMigration cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TransactionTypeMigrationDTO result = transactionTypeMigrationService.save(transactionTypeMigrationDTO);
        return ResponseEntity
            .created(new URI("/api/transaction-type-migrations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /transaction-type-migrations/:id} : Updates an existing transactionTypeMigration.
     *
     * @param id the id of the transactionTypeMigrationDTO to save.
     * @param transactionTypeMigrationDTO the transactionTypeMigrationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transactionTypeMigrationDTO,
     * or with status {@code 400 (Bad Request)} if the transactionTypeMigrationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the transactionTypeMigrationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/transaction-type-migrations/{id}")
    public ResponseEntity<TransactionTypeMigrationDTO> updateTransactionTypeMigration(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody TransactionTypeMigrationDTO transactionTypeMigrationDTO
    ) throws URISyntaxException {
        log.debug("REST request to update TransactionTypeMigration : {}, {}", id, transactionTypeMigrationDTO);
        if (transactionTypeMigrationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, transactionTypeMigrationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!transactionTypeMigrationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TransactionTypeMigrationDTO result = transactionTypeMigrationService.update(transactionTypeMigrationDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, transactionTypeMigrationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /transaction-type-migrations/:id} : Partial updates given fields of an existing transactionTypeMigration, field will ignore if it is null
     *
     * @param id the id of the transactionTypeMigrationDTO to save.
     * @param transactionTypeMigrationDTO the transactionTypeMigrationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transactionTypeMigrationDTO,
     * or with status {@code 400 (Bad Request)} if the transactionTypeMigrationDTO is not valid,
     * or with status {@code 404 (Not Found)} if the transactionTypeMigrationDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the transactionTypeMigrationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/transaction-type-migrations/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<TransactionTypeMigrationDTO> partialUpdateTransactionTypeMigration(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody TransactionTypeMigrationDTO transactionTypeMigrationDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update TransactionTypeMigration partially : {}, {}", id, transactionTypeMigrationDTO);
        if (transactionTypeMigrationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, transactionTypeMigrationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!transactionTypeMigrationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TransactionTypeMigrationDTO> result = transactionTypeMigrationService.partialUpdate(transactionTypeMigrationDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, transactionTypeMigrationDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /transaction-type-migrations} : get all the transactionTypeMigrations.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transactionTypeMigrations in body.
     */
    @GetMapping("/transaction-type-migrations")
    public List<TransactionTypeMigrationDTO> getAllTransactionTypeMigrations() {
        log.debug("REST request to get all TransactionTypeMigrations");
        return transactionTypeMigrationService.findAll();
    }

    /**
     * {@code GET  /transaction-type-migrations/:id} : get the "id" transactionTypeMigration.
     *
     * @param id the id of the transactionTypeMigrationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the transactionTypeMigrationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/transaction-type-migrations/{id}")
    public ResponseEntity<TransactionTypeMigrationDTO> getTransactionTypeMigration(@PathVariable Long id) {
        log.debug("REST request to get TransactionTypeMigration : {}", id);
        Optional<TransactionTypeMigrationDTO> transactionTypeMigrationDTO = transactionTypeMigrationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(transactionTypeMigrationDTO);
    }

    /**
     * {@code DELETE  /transaction-type-migrations/:id} : delete the "id" transactionTypeMigration.
     *
     * @param id the id of the transactionTypeMigrationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/transaction-type-migrations/{id}")
    public ResponseEntity<Void> deleteTransactionTypeMigration(@PathVariable Long id) {
        log.debug("REST request to delete TransactionTypeMigration : {}", id);
        transactionTypeMigrationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

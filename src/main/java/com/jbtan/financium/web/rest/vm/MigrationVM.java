package com.jbtan.financium.web.rest.vm;

import lombok.Data;

@Data
public class MigrationVM {

    private Long userId;
}

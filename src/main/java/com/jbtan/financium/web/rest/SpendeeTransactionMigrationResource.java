package com.jbtan.financium.web.rest;

import com.jbtan.financium.repository.SpendeeTransactionMigrationRepository;
import com.jbtan.financium.service.SpendeeTransactionMigrationService;
import com.jbtan.financium.service.dto.SpendeeTransactionMigrationDTO;
import com.jbtan.financium.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jbtan.financium.domain.SpendeeTransactionMigration}.
 */
@RestController
@RequestMapping("/api")
public class SpendeeTransactionMigrationResource {

    private final Logger log = LoggerFactory.getLogger(SpendeeTransactionMigrationResource.class);

    private static final String ENTITY_NAME = "spendeeTransactionMigration";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SpendeeTransactionMigrationService spendeeTransactionMigrationService;

    private final SpendeeTransactionMigrationRepository spendeeTransactionMigrationRepository;

    public SpendeeTransactionMigrationResource(
        SpendeeTransactionMigrationService spendeeTransactionMigrationService,
        SpendeeTransactionMigrationRepository spendeeTransactionMigrationRepository
    ) {
        this.spendeeTransactionMigrationService = spendeeTransactionMigrationService;
        this.spendeeTransactionMigrationRepository = spendeeTransactionMigrationRepository;
    }

    /**
     * {@code POST  /spendee-transaction-migrations} : Create a new spendeeTransactionMigration.
     *
     * @param spendeeTransactionMigrationDTO the spendeeTransactionMigrationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new spendeeTransactionMigrationDTO, or with status {@code 400 (Bad Request)} if the spendeeTransactionMigration has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/spendee-transaction-migrations")
    public ResponseEntity<SpendeeTransactionMigrationDTO> createSpendeeTransactionMigration(
        @Valid @RequestBody SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO
    ) throws URISyntaxException {
        log.debug("REST request to save SpendeeTransactionMigration : {}", spendeeTransactionMigrationDTO);
        if (spendeeTransactionMigrationDTO.getId() != null) {
            throw new BadRequestAlertException("A new spendeeTransactionMigration cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SpendeeTransactionMigrationDTO result = spendeeTransactionMigrationService.save(spendeeTransactionMigrationDTO);
        return ResponseEntity
            .created(new URI("/api/spendee-transaction-migrations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /spendee-transaction-migrations/:id} : Updates an existing spendeeTransactionMigration.
     *
     * @param id the id of the spendeeTransactionMigrationDTO to save.
     * @param spendeeTransactionMigrationDTO the spendeeTransactionMigrationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated spendeeTransactionMigrationDTO,
     * or with status {@code 400 (Bad Request)} if the spendeeTransactionMigrationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the spendeeTransactionMigrationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/spendee-transaction-migrations/{id}")
    public ResponseEntity<SpendeeTransactionMigrationDTO> updateSpendeeTransactionMigration(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO
    ) throws URISyntaxException {
        log.debug("REST request to update SpendeeTransactionMigration : {}, {}", id, spendeeTransactionMigrationDTO);
        if (spendeeTransactionMigrationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, spendeeTransactionMigrationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!spendeeTransactionMigrationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        SpendeeTransactionMigrationDTO result = spendeeTransactionMigrationService.update(spendeeTransactionMigrationDTO);
        return ResponseEntity
            .ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, spendeeTransactionMigrationDTO.getId().toString())
            )
            .body(result);
    }

    /**
     * {@code PATCH  /spendee-transaction-migrations/:id} : Partial updates given fields of an existing spendeeTransactionMigration, field will ignore if it is null
     *
     * @param id the id of the spendeeTransactionMigrationDTO to save.
     * @param spendeeTransactionMigrationDTO the spendeeTransactionMigrationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated spendeeTransactionMigrationDTO,
     * or with status {@code 400 (Bad Request)} if the spendeeTransactionMigrationDTO is not valid,
     * or with status {@code 404 (Not Found)} if the spendeeTransactionMigrationDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the spendeeTransactionMigrationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/spendee-transaction-migrations/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<SpendeeTransactionMigrationDTO> partialUpdateSpendeeTransactionMigration(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update SpendeeTransactionMigration partially : {}, {}", id, spendeeTransactionMigrationDTO);
        if (spendeeTransactionMigrationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, spendeeTransactionMigrationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!spendeeTransactionMigrationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<SpendeeTransactionMigrationDTO> result = spendeeTransactionMigrationService.partialUpdate(spendeeTransactionMigrationDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, spendeeTransactionMigrationDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /spendee-transaction-migrations} : get all the spendeeTransactionMigrations.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of spendeeTransactionMigrations in body.
     */
    @GetMapping("/spendee-transaction-migrations")
    public List<SpendeeTransactionMigrationDTO> getAllSpendeeTransactionMigrations() {
        log.debug("REST request to get all SpendeeTransactionMigrations");
        return spendeeTransactionMigrationService.findAll();
    }

    /**
     * {@code GET  /spendee-transaction-migrations/:id} : get the "id" spendeeTransactionMigration.
     *
     * @param id the id of the spendeeTransactionMigrationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the spendeeTransactionMigrationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/spendee-transaction-migrations/{id}")
    public ResponseEntity<SpendeeTransactionMigrationDTO> getSpendeeTransactionMigration(@PathVariable Long id) {
        log.debug("REST request to get SpendeeTransactionMigration : {}", id);
        Optional<SpendeeTransactionMigrationDTO> spendeeTransactionMigrationDTO = spendeeTransactionMigrationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(spendeeTransactionMigrationDTO);
    }

    /**
     * {@code DELETE  /spendee-transaction-migrations/:id} : delete the "id" spendeeTransactionMigration.
     *
     * @param id the id of the spendeeTransactionMigrationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/spendee-transaction-migrations/{id}")
    public ResponseEntity<Void> deleteSpendeeTransactionMigration(@PathVariable Long id) {
        log.debug("REST request to delete SpendeeTransactionMigration : {}", id);
        spendeeTransactionMigrationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

package com.jbtan.financium.web.rest;

import com.jbtan.financium.decorator.DashboardBusinessDecorator;
import com.jbtan.financium.domain.enumeration.DashboardDurationType;
import com.jbtan.financium.projection.TransactionMonthlySummaryProjection;
import com.jbtan.financium.service.dto.DashboardGroupedSummaryDTO;
import com.jbtan.financium.service.dto.DashboardSummaryDTO;
import com.jbtan.financium.service.dto.TransactionDTO;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.PaginationUtil;

@RestController
@RequestMapping("/api/dashboard")
@Slf4j
@RequiredArgsConstructor
public class DashboardResource {

    @Qualifier("decorator")
    private final DashboardBusinessDecorator dashboardBusinessDecorator;

    @GetMapping("/summary/{type}")
    public ResponseEntity<DashboardSummaryDTO> getDashboardSummary(
        @PathVariable("type") DashboardDurationType type,
        @RequestParam("walletId") Long walletId,
        @RequestParam("start") LocalDate start,
        @RequestParam("end") LocalDate end
    ) {
        return ResponseEntity.ok(dashboardBusinessDecorator.getSummary(walletId, start, end));
    }

    @GetMapping("/transactions")
    public ResponseEntity<List<TransactionDTO>> getDashboardTransactionList(
        @RequestParam("walletId") Long walletId,
        @RequestParam("start") LocalDate start,
        @RequestParam("end") LocalDate end,
        Pageable pageable
    ) {
        Page<TransactionDTO> page = dashboardBusinessDecorator.getCurrentUserTransactionList(walletId, start, end, pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);

        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/last-five-transactions")
    public ResponseEntity<List<TransactionDTO>> getLastFiveTransactions(@RequestParam("walletId") Long walletId) {
        Page<TransactionDTO> page = dashboardBusinessDecorator.getCurrentUserLastTransactionList(walletId, 5);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);

        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/summary/top-four-spendings-category")
    public ResponseEntity<List<DashboardGroupedSummaryDTO>> getTopFourSpendingsCategory(
        @RequestParam("walletId") Long walletId,
        @RequestParam("start") LocalDate start,
        @RequestParam("end") LocalDate end
    ) {
        return ResponseEntity.ok(dashboardBusinessDecorator.getTopFourSpendings(walletId, start, end));
    }

    @GetMapping("/summary/income-expenses")
    public ResponseEntity<Map<String, List<TransactionMonthlySummaryProjection>>> getMonthlySummaryFromDateRange(
        @RequestParam("walletId") Long walletId,
        @RequestParam("start") LocalDate start,
        @RequestParam("end") LocalDate end
    ) {
        return ResponseEntity.ok(dashboardBusinessDecorator.getMonthlySummaryFromDateRange(walletId, start, end));
    }
}

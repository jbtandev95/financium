package com.jbtan.financium.web.rest.middleware;

import com.jbtan.financium.web.rest.response.ResponseFormat;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

//@ControllerAdvice
public class CustomResponseBodyAdvice<T> implements ResponseBodyAdvice<ResponseFormat<T>> {

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    @Override
    public ResponseFormat<T> beforeBodyWrite(
        ResponseFormat<T> body,
        MethodParameter returnType,
        MediaType selectedContentType,
        Class<? extends HttpMessageConverter<?>> selectedConverterType,
        ServerHttpRequest request,
        ServerHttpResponse response
    ) {
        body.setStatusCode("200");
        return body;
    }
}

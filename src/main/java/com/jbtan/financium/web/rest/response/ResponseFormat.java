package com.jbtan.financium.web.rest.response;

import java.io.Serializable;
import lombok.Data;

@Data
public class ResponseFormat<T> implements Serializable {

    private String statusCode;
    private String message;
    private Long id;
    private T response;

    public ResponseFormat(Long id) {
        this.id = id;
    }

    public ResponseFormat(T response) {
        this.response = response;
    }
}

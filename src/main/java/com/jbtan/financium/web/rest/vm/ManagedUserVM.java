package com.jbtan.financium.web.rest.vm;

import com.jbtan.financium.domain.enumeration.CurrencyCode;
import com.jbtan.financium.service.dto.AdminUserDTO;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * View Model extending the AdminUserDTO, which is meant to be used in the user
 * management UI.
 */
@Data
public class ManagedUserVM extends AdminUserDTO {

    public static final int PASSWORD_MIN_LENGTH = 4;

    public static final int PASSWORD_MAX_LENGTH = 100;

    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;

    private String walletName;

    private CurrencyCode currency;

    public ManagedUserVM() {
        // Empty constructor needed for Jackson.
    }
}

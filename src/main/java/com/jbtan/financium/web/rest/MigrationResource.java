package com.jbtan.financium.web.rest;

import com.jbtan.financium.business.JobBusiness;
import com.jbtan.financium.domain.DashboardView;
import com.jbtan.financium.domain.enumeration.MigrationSource;
import com.jbtan.financium.service.DashboardViewService;
import com.jbtan.financium.service.UserService;
import com.jbtan.financium.web.rest.vm.MigrationVM;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/migration")
@Slf4j
@RequiredArgsConstructor
public class MigrationResource {

    private final JobBusiness jobBusiness;

    private final UserService userService;

    private final DashboardViewService dashboardViewService;

    @PostMapping("/spendee")
    public ResponseEntity<?> migrationDataFromSpendee(@RequestBody MigrationVM migrationVM) {
        //		Long userId = userService.getUserId(SecurityUtils.getCurrentUserLogin().get());
        Long userId = migrationVM.getUserId();

        jobBusiness.migrateDataFromOthers(MigrationSource.SPENDEE, userId);

        return ResponseEntity.ok("Submitted!");
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<List<DashboardView>> get(@PathVariable Long id) {
        List<DashboardView> dashboardData = dashboardViewService.findByUserId(id);

        return ResponseEntity.ok(dashboardData);
    }
}

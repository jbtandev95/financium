package com.jbtan.financium.service;

import com.jbtan.financium.domain.enumeration.TransactionCategory;
import com.jbtan.financium.domain.enumeration.TransactionStatus;
import com.jbtan.financium.projection.TransactionMonthlySummaryProjection;
import com.jbtan.financium.service.dto.DashboardGroupedSummaryDTO;
import com.jbtan.financium.service.dto.DashboardSummaryDTO;
import com.jbtan.financium.service.dto.TransactionDTO;
import com.jbtan.financium.service.dto.WalletDTO;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing
 * {@link com.jbtan.financium.domain.Transaction}.
 */
public interface TransactionService {
    /**
     * Save a transaction.
     *
     * @param transactionDTO the entity to save.
     * @return the persisted entity.
     */
    TransactionDTO save(TransactionDTO transactionDTO);

    /**
     * Updates a transaction.
     *
     * @param transactionDTO the entity to update.
     * @return the persisted entity.
     */
    TransactionDTO update(TransactionDTO transactionDTO);

    /**
     * Partially updates a transaction.
     *
     * @param transactionDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<TransactionDTO> partialUpdate(TransactionDTO transactionDTO);

    /**
     * Get all the transactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TransactionDTO> findAll(Pageable pageable);

    /**
     * Get the "id" transaction.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TransactionDTO> findOne(Long id);

    /**
     * Delete the "id" transaction.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    DashboardSummaryDTO getCurrentUserSummaryByWalletIdAndDateTime(Long walletId, ZonedDateTime start, ZonedDateTime end);

    List<DashboardGroupedSummaryDTO> getCurrentUserGroupedSummaryByWalletIdAndDateTime(
        Long walletId,
        TransactionCategory category,
        ZonedDateTime start,
        ZonedDateTime end,
        Pageable pageable
    );

    Page<TransactionDTO> getCurrentUserTransactionList(
        Long walletId,
        TransactionStatus status,
        ZonedDateTime start,
        ZonedDateTime end,
        Pageable pageable
    );

    Page<TransactionDTO> getCurrentUserLatestTransactionList(Long walletId, TransactionStatus status, int size);

    List<TransactionMonthlySummaryProjection> getMonthlySummaryFromDateRange(WalletDTO wallet, ZonedDateTime start, ZonedDateTime end);
}

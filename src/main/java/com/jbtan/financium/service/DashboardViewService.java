package com.jbtan.financium.service;

import com.jbtan.financium.domain.DashboardView;
import java.util.List;

public interface DashboardViewService {
    List<DashboardView> findByUserId(Long userId);
}

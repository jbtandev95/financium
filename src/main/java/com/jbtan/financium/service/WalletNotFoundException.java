package com.jbtan.financium.service;

public class WalletNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public WalletNotFoundException() {
        super("Wallet Not Found!");
    }
}

package com.jbtan.financium.service.mapper;

import com.jbtan.financium.domain.TransactionType;
import com.jbtan.financium.domain.User;
import com.jbtan.financium.service.dto.TransactionTypeDTO;
import com.jbtan.financium.service.dto.UserDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link TransactionType} and its DTO
 * {@link TransactionTypeDTO}.
 */
@Mapper(componentModel = "spring")
public interface TransactionTypeMapper extends EntityMapper<TransactionTypeDTO, TransactionType> {
    @Override
    @Mapping(target = "userId", source = "user.id")
    TransactionTypeDTO toDto(TransactionType s);

    @Override
    @Mapping(target = "user.id", source = "userId")
    TransactionType toEntity(TransactionTypeDTO s);

    @Named("userId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    UserDTO toDtoUserId(User user);

    default TransactionType fromId(Long id) {
        if (id == null) {
            return null;
        }
        TransactionType trnType = new TransactionType();
        trnType.setId(id);
        return trnType;
    }
}

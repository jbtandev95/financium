package com.jbtan.financium.service.mapper;

import com.jbtan.financium.domain.User;
import com.jbtan.financium.domain.Wallet;
import com.jbtan.financium.service.dto.UserDTO;
import com.jbtan.financium.service.dto.WalletDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

/**
 * Mapper for the entity {@link Wallet} and its DTO {@link WalletDTO}.
 */
@Mapper(componentModel = "spring")
public interface WalletMapper extends EntityMapper<WalletDTO, Wallet> {
    @Override
    @Mapping(target = "user", source = "user", qualifiedByName = "userId")
    WalletDTO toDto(Wallet s);

    @Named("userId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    UserDTO toDtoUserId(User user);

    default Wallet fromId(Long id) {
        if (id == null) {
            return null;
        }
        Wallet wallet = new Wallet();
        wallet.setId(id);
        return wallet;
    }
}

package com.jbtan.financium.service.mapper;

import com.jbtan.financium.domain.Transaction;
import com.jbtan.financium.service.dto.TransactionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Transaction} and its DTO {@link TransactionDTO}.
 */
@Mapper(componentModel = "spring", uses = { WalletMapper.class, TransactionTypeMapper.class, UserMapper.class })
public interface TransactionMapper extends EntityMapper<TransactionDTO, Transaction> {
    @Override
    @Mapping(target = "userId", source = "user.id")
    @Mapping(target = "walletId", source = "wallet.id")
    @Mapping(target = "transactionTypeId", source = "transactionType.id")
    TransactionDTO toDto(Transaction s);

    @Override
    @Mapping(target = "user", source = "userId")
    @Mapping(target = "wallet", source = "walletId")
    @Mapping(target = "transactionType", source = "transactionTypeId")
    Transaction toEntity(TransactionDTO transactionDTO);
}

package com.jbtan.financium.service.mapper;

import com.jbtan.financium.domain.SpendeeTransactionMigration;
import com.jbtan.financium.service.dto.SpendeeTransactionMigrationDTO;
import com.jbtan.financium.service.dto.TransactionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link SpendeeTransactionMigration} and its DTO
 * {@link SpendeeTransactionMigrationDTO}.
 */
@Mapper(componentModel = "spring")
public interface SpendeeTransactionMigrationMapper extends EntityMapper<SpendeeTransactionMigrationDTO, SpendeeTransactionMigration> {
    @Override
    @Mapping(target = "userId", source = "user.id")
    SpendeeTransactionMigrationDTO toDto(SpendeeTransactionMigration s);

    @Override
    @Mapping(target = "user.id", source = "userId")
    SpendeeTransactionMigration toEntity(SpendeeTransactionMigrationDTO s);

    @Mapping(target = "name", source = "note")
    TransactionDTO toTransactionDTO(SpendeeTransactionMigration s);
}

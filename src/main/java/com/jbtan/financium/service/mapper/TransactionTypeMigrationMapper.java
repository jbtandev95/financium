package com.jbtan.financium.service.mapper;

import com.jbtan.financium.domain.TransactionTypeMigration;
import com.jbtan.financium.domain.User;
import com.jbtan.financium.service.dto.TransactionTypeMigrationDTO;
import com.jbtan.financium.service.dto.UserDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link TransactionTypeMigration} and its DTO {@link TransactionTypeMigrationDTO}.
 */
@Mapper(componentModel = "spring")
public interface TransactionTypeMigrationMapper extends EntityMapper<TransactionTypeMigrationDTO, TransactionTypeMigration> {
    @Mapping(target = "user", source = "user", qualifiedByName = "userId")
    TransactionTypeMigrationDTO toDto(TransactionTypeMigration s);

    @Named("userId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    UserDTO toDtoUserId(User user);
}

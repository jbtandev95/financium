package com.jbtan.financium.service;

import com.jbtan.financium.domain.SpendeeTransactionMigration;
import com.jbtan.financium.service.dto.SpendeeTransactionMigrationDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing
 * {@link com.jbtan.financium.domain.SpendeeTransactionMigration}.
 */
public interface SpendeeTransactionMigrationService {
    /**
     * Save a spendeeTransactionMigration.
     *
     * @param spendeeTransactionMigrationDTO the entity to save.
     * @return the persisted entity.
     */
    SpendeeTransactionMigrationDTO save(SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO);

    /**
     * Updates a spendeeTransactionMigration.
     *
     * @param spendeeTransactionMigrationDTO the entity to update.
     * @return the persisted entity.
     */
    SpendeeTransactionMigrationDTO update(SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO);

    /**
     * Partially updates a spendeeTransactionMigration.
     *
     * @param spendeeTransactionMigrationDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<SpendeeTransactionMigrationDTO> partialUpdate(SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO);

    /**
     * Get all the spendeeTransactionMigrations.
     *
     * @return the list of entities.
     */
    List<SpendeeTransactionMigrationDTO> findAll();

    /**
     * Get the "id" spendeeTransactionMigration.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SpendeeTransactionMigrationDTO> findOne(Long id);

    /**
     * Delete the "id" spendeeTransactionMigration.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     *
     * @param userId
     * @return
     */
    List<String> findDistinctCategoryByUserId(Long userId);

    /**
     *
     * @param categoryName
     * @return
     */
    List<SpendeeTransactionMigration> findAllByCategoryName(String categoryName);
}

package com.jbtan.financium.service;

import com.jbtan.financium.service.dto.TransactionTypeMigrationDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.jbtan.financium.domain.TransactionTypeMigration}.
 */
public interface TransactionTypeMigrationService {
    /**
     * Save a transactionTypeMigration.
     *
     * @param transactionTypeMigrationDTO the entity to save.
     * @return the persisted entity.
     */
    TransactionTypeMigrationDTO save(TransactionTypeMigrationDTO transactionTypeMigrationDTO);

    /**
     * Updates a transactionTypeMigration.
     *
     * @param transactionTypeMigrationDTO the entity to update.
     * @return the persisted entity.
     */
    TransactionTypeMigrationDTO update(TransactionTypeMigrationDTO transactionTypeMigrationDTO);

    /**
     * Partially updates a transactionTypeMigration.
     *
     * @param transactionTypeMigrationDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<TransactionTypeMigrationDTO> partialUpdate(TransactionTypeMigrationDTO transactionTypeMigrationDTO);

    /**
     * Get all the transactionTypeMigrations.
     *
     * @return the list of entities.
     */
    List<TransactionTypeMigrationDTO> findAll();

    /**
     * Get the "id" transactionTypeMigration.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TransactionTypeMigrationDTO> findOne(Long id);

    /**
     * Delete the "id" transactionTypeMigration.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}

package com.jbtan.financium.service.dto;

import java.io.Serializable;
import java.time.Instant;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

/**
 * A DTO for the {@link com.jbtan.financium.domain.TransactionType} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
@Data
@Builder
public class TransactionTypeDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Instant createdDate;

    @NotNull
    private String createdBy;

    @NotNull
    private Instant lastModifiedDate;

    @NotNull
    private String lastModifiedBy;

    @NotNull
    private Long userId;
}

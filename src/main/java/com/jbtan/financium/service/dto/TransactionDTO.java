package com.jbtan.financium.service.dto;

import com.jbtan.financium.domain.enumeration.CurrencyCode;
import com.jbtan.financium.domain.enumeration.TransactionCategory;
import com.jbtan.financium.domain.enumeration.TransactionStatus;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * A DTO for the {@link com.jbtan.financium.domain.Transaction} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
@Data
public class TransactionDTO implements Serializable {

    public interface TransactionPutMapping {}

    public interface TransactionPostMapping {}

    @NotNull(groups = { TransactionPutMapping.class })
    private Long id;

    @NotNull(groups = { TransactionPostMapping.class })
    private String name;

    @NotNull(groups = { TransactionPostMapping.class })
    private TransactionCategory category;

    @NotNull(groups = { TransactionPostMapping.class })
    private BigDecimal amount;

    private CurrencyCode currency;

    @NotNull(groups = { TransactionPostMapping.class })
    private ZonedDateTime transactionDate;

    private TransactionStatus status;

    private Instant createdDate;

    private String createdBy;

    private Instant lastModifiedDate;

    private String lastModifiedBy;

    private Long userId;

    @NotNull(groups = { TransactionPostMapping.class })
    private Long walletId;

    @NotNull(groups = { TransactionPostMapping.class })
    private Long transactionTypeId;
}

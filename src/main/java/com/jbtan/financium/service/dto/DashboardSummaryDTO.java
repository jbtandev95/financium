package com.jbtan.financium.service.dto;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import lombok.Data;

@Data
public class DashboardSummaryDTO {

    private BigDecimal totalExpenses;

    private BigDecimal totalIncome;

    private BigDecimal totalPeriodChange;

    private List<DashboardGroupedSummaryDTO> incomeGroupedSummaries;

    private List<DashboardGroupedSummaryDTO> expensesGroupedSummaries;

    private ZonedDateTime from;

    private ZonedDateTime end;

    public DashboardSummaryDTO() {}

    public DashboardSummaryDTO(BigDecimal totalExpenses, BigDecimal totalIncome) {
        this.totalExpenses = totalExpenses;
        this.totalIncome = totalIncome;
        this.totalPeriodChange = totalIncome.add(totalExpenses);
    }
}

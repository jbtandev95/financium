package com.jbtan.financium.service.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class SpendeeMigrationDTO {

    private String date;
    private String wallet;
    private String categoryName;
    private BigDecimal amount;
    private String currency;
    private String note;
    private String labels;
    private String author;
    private String type;
}

package com.jbtan.financium.service.dto;

import com.jbtan.financium.domain.enumeration.CurrencyCode;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * A DTO for the {@link com.jbtan.financium.domain.SpendeeTransactionMigration}
 * entity.
 */
@Schema(description = "Date,Wallet,Type,\"Category name\",Amount,Currency,Note,Labels,Author")
@SuppressWarnings("common-java:DuplicatedBlocks")
@Data
public class SpendeeTransactionMigrationDTO implements Serializable {

    private Long id;

    @NotNull
    private String categoryName;

    @NotNull
    private CurrencyCode currency;

    @NotNull
    private String type;

    @NotNull
    private BigDecimal amount;

    @NotNull
    private String note;

    @NotNull
    private ZonedDateTime transactionDate;

    private Long userId;
}

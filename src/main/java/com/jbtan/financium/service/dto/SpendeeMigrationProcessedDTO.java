package com.jbtan.financium.service.dto;

import java.util.List;
import lombok.Data;

@Data
public class SpendeeMigrationProcessedDTO extends SpendeeMigrationDTO {

    private List<String> transactionTypes;
    private List<String> SpendeeMigrationDTO;
}

package com.jbtan.financium.service.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class DashboardGroupedSummaryDTO {

    private String name;
    private BigDecimal totalAmount;
    private Long count;

    public DashboardGroupedSummaryDTO() {}

    public DashboardGroupedSummaryDTO(String name, BigDecimal totalAmount, Long count) {
        this.name = name;
        this.totalAmount = totalAmount;
        this.count = count;
    }
}

package com.jbtan.financium.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.jbtan.financium.domain.TransactionTypeMigration} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TransactionTypeMigrationDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private UserDTO user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TransactionTypeMigrationDTO)) {
            return false;
        }

        TransactionTypeMigrationDTO transactionTypeMigrationDTO = (TransactionTypeMigrationDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, transactionTypeMigrationDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TransactionTypeMigrationDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", user=" + getUser() +
            "}";
    }
}

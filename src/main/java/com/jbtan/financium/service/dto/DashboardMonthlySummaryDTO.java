package com.jbtan.financium.service.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class DashboardMonthlySummaryDTO {

    private BigDecimal totalIncome;

    private BigDecimal totalExpenses;

    private String month;
}

package com.jbtan.financium.service.impl;

import com.jbtan.financium.domain.TransactionType;
import com.jbtan.financium.repository.TransactionTypeRepository;
import com.jbtan.financium.service.TransactionTypeService;
import com.jbtan.financium.service.dto.TransactionTypeDTO;
import com.jbtan.financium.service.mapper.TransactionTypeMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link TransactionType}.
 */
@Service
@Transactional(readOnly = true)
public class TransactionTypeServiceImpl implements TransactionTypeService {

    private final Logger log = LoggerFactory.getLogger(TransactionTypeServiceImpl.class);

    private final TransactionTypeRepository transactionTypeRepository;

    private final TransactionTypeMapper transactionTypeMapper;

    public TransactionTypeServiceImpl(TransactionTypeRepository transactionTypeRepository, TransactionTypeMapper transactionTypeMapper) {
        this.transactionTypeRepository = transactionTypeRepository;
        this.transactionTypeMapper = transactionTypeMapper;
    }

    @Override
    @Transactional(readOnly = false)
    public TransactionTypeDTO save(TransactionTypeDTO transactionTypeDTO) {
        log.debug("Request to save TransactionType : {}", transactionTypeDTO);
        TransactionType transactionType = transactionTypeMapper.toEntity(transactionTypeDTO);
        transactionType = transactionTypeRepository.save(transactionType);
        log.debug("Request to save TransactionType : {}", transactionType);
        return transactionTypeMapper.toDto(transactionType);
    }

    @Override
    @Transactional(readOnly = false)
    public TransactionTypeDTO update(TransactionTypeDTO transactionTypeDTO) {
        log.debug("Request to update TransactionType : {}", transactionTypeDTO);
        TransactionType transactionType = transactionTypeMapper.toEntity(transactionTypeDTO);
        transactionType = transactionTypeRepository.save(transactionType);
        return transactionTypeMapper.toDto(transactionType);
    }

    @Override
    @Transactional(readOnly = false)
    public Optional<TransactionTypeDTO> partialUpdate(TransactionTypeDTO transactionTypeDTO) {
        log.debug("Request to partially update TransactionType : {}", transactionTypeDTO);

        return transactionTypeRepository
            .findById(transactionTypeDTO.getId())
            .map(existingTransactionType -> {
                transactionTypeMapper.partialUpdate(existingTransactionType, transactionTypeDTO);

                return existingTransactionType;
            })
            .map(transactionTypeRepository::save)
            .map(transactionTypeMapper::toDto);
    }

    @Override
    public List<TransactionTypeDTO> findAll() {
        log.debug("Request to get all TransactionTypes");
        return transactionTypeRepository
            .findAll()
            .stream()
            .map(transactionTypeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Optional<TransactionTypeDTO> findOne(Long id) {
        log.debug("Request to get TransactionType : {}", id);
        return transactionTypeRepository.findById(id).map(transactionTypeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Long id) {
        log.debug("Request to delete TransactionType : {}", id);
        transactionTypeRepository.deleteById(id);
    }

    @Override
    public List<TransactionType> findByTransactionTypeListByUserId(Long userId) {
        return transactionTypeRepository.findByTransactionTypeListByUserId(userId);
    }
}

package com.jbtan.financium.service.impl;

import com.jbtan.financium.domain.DashboardView;
import com.jbtan.financium.repository.DashboardViewRepository;
import com.jbtan.financium.service.DashboardViewService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class DashboardViewServiceImpl implements DashboardViewService {

    private final DashboardViewRepository dashboardViewRepository;

    @Override
    public List<DashboardView> findByUserId(Long userId) {
        return dashboardViewRepository.findByUserId(userId);
    }
}

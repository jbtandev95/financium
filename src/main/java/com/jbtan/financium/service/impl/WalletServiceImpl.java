package com.jbtan.financium.service.impl;

import com.jbtan.financium.domain.Wallet;
import com.jbtan.financium.domain.enumeration.CurrencyCode;
import com.jbtan.financium.repository.WalletRepository;
import com.jbtan.financium.security.SecurityUtils;
import com.jbtan.financium.service.WalletService;
import com.jbtan.financium.service.dto.WalletDTO;
import com.jbtan.financium.service.mapper.WalletMapper;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Wallet}.
 */
@Service
@Transactional(readOnly = true)
@Slf4j
@RequiredArgsConstructor
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;

    private final WalletMapper walletMapper;

    @Override
    @Transactional(readOnly = false)
    public WalletDTO save(WalletDTO walletDTO) {
        log.debug("Request to save Wallet : {}", walletDTO);
        Wallet wallet = walletMapper.toEntity(walletDTO);
        wallet = walletRepository.save(wallet);
        return walletMapper.toDto(wallet);
    }

    @Override
    @Transactional(readOnly = false)
    public WalletDTO update(WalletDTO walletDTO) {
        log.debug("Request to update Wallet : {}", walletDTO);
        Wallet wallet = walletMapper.toEntity(walletDTO);
        wallet = walletRepository.save(wallet);
        return walletMapper.toDto(wallet);
    }

    @Override
    @Transactional(readOnly = false)
    public Optional<WalletDTO> partialUpdate(WalletDTO walletDTO) {
        log.debug("Request to partially update Wallet : {}", walletDTO);

        return walletRepository
            .findById(walletDTO.getId())
            .map(existingWallet -> {
                walletMapper.partialUpdate(existingWallet, walletDTO);

                return existingWallet;
            })
            .map(walletRepository::save)
            .map(walletMapper::toDto);
    }

    @Override
    public List<WalletDTO> findAll() {
        log.debug("Request to get all Wallets");
        return walletRepository.findAll().stream().map(walletMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Optional<WalletDTO> findOne(Long id) {
        log.debug("Request to get Wallet : {}", id);
        return walletRepository.findById(id).map(walletMapper::toDto);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Long id) {
        log.debug("Request to delete Wallet : {}", id);
        walletRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = false)
    public WalletDTO register(String walletName, CurrencyCode currency) {
        Wallet wallet = new Wallet().name(walletName).currency(currency).amount(BigDecimal.ZERO);

        return walletMapper.toDto(walletRepository.save(wallet));
    }

    @Override
    public Optional<WalletDTO> findCurrentUserWalletByWalletId(Long walletId) {
        log.info("current principal {}", SecurityUtils.getCurrentUserLogin());
        return walletRepository.findCurrentUserWalletByWalletId(walletId).map(wl -> walletMapper.toDto(wl));
    }

    @Override
    public Optional<Long> findIfWalletIdBelongsToCurrentUser(Long walletId) {
        return walletRepository.findIfWalletIdBelongsToCurrentUser(walletId);
    }
}

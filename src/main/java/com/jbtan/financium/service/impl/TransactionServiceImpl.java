package com.jbtan.financium.service.impl;

import com.jbtan.financium.domain.Transaction;
import com.jbtan.financium.domain.enumeration.TransactionCategory;
import com.jbtan.financium.domain.enumeration.TransactionStatus;
import com.jbtan.financium.projection.TransactionMonthlySummaryProjection;
import com.jbtan.financium.repository.TransactionRepository;
import com.jbtan.financium.service.TransactionService;
import com.jbtan.financium.service.dto.DashboardGroupedSummaryDTO;
import com.jbtan.financium.service.dto.DashboardSummaryDTO;
import com.jbtan.financium.service.dto.TransactionDTO;
import com.jbtan.financium.service.dto.WalletDTO;
import com.jbtan.financium.service.mapper.TransactionMapper;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Transaction}.
 */
@Service
@Transactional(readOnly = true)
@Slf4j
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;

    private final TransactionMapper transactionMapper;

    @Override
    @Transactional(readOnly = false)
    public TransactionDTO save(TransactionDTO transactionDTO) {
        log.debug("Request to save Transaction : {}", transactionDTO);
        Transaction transaction = transactionMapper.toEntity(transactionDTO);
        log.info("Request to save Transaction after mapped : {}", transaction);
        transaction = transactionRepository.save(transaction);
        return transactionMapper.toDto(transaction);
    }

    @Override
    @Transactional(readOnly = false)
    public TransactionDTO update(TransactionDTO transactionDTO) {
        log.debug("Request to update Transaction : {}", transactionDTO);
        Transaction transaction = transactionMapper.toEntity(transactionDTO);
        transaction = transactionRepository.save(transaction);
        return transactionMapper.toDto(transaction);
    }

    @Override
    @Transactional(readOnly = false)
    public Optional<TransactionDTO> partialUpdate(TransactionDTO transactionDTO) {
        log.debug("Request to partially update Transaction : {}", transactionDTO);

        return transactionRepository
            .findById(transactionDTO.getId())
            .map(existingTransaction -> {
                transactionMapper.partialUpdate(existingTransaction, transactionDTO);

                return existingTransaction;
            })
            .map(transactionRepository::save)
            .map(transactionMapper::toDto);
    }

    @Override
    public Page<TransactionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Transactions");
        return transactionRepository.findAll(pageable).map(transactionMapper::toDto);
    }

    @Override
    public Optional<TransactionDTO> findOne(Long id) {
        log.debug("Request to get Transaction : {}", id);
        return transactionRepository.findById(id).map(transactionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Long id) {
        log.debug("Request to delete Transaction : {}", id);
        transactionRepository
            .findById(id)
            .ifPresent(t -> {
                t.status(TransactionStatus.D);

                transactionRepository.save(t);
            });
    }

    @Override
    public DashboardSummaryDTO getCurrentUserSummaryByWalletIdAndDateTime(Long walletId, ZonedDateTime start, ZonedDateTime end) {
        DashboardSummaryDTO summary = transactionRepository.getCurrentUserSummaryByWalletIdAndDateTime(walletId, start, end);

        summary.setFrom(start);
        summary.setEnd(end);
        return summary;
    }

    @Override
    public List<DashboardGroupedSummaryDTO> getCurrentUserGroupedSummaryByWalletIdAndDateTime(
        Long walletId,
        TransactionCategory category,
        ZonedDateTime start,
        ZonedDateTime end,
        Pageable pageable
    ) {
        List<DashboardGroupedSummaryDTO> groupedSummaries = transactionRepository.getCurrentUserGroupedSummariesByWalletIdAndDateTime(
            walletId,
            category,
            start,
            end,
            pageable
        );
        return groupedSummaries;
    }

    @Override
    public Page<TransactionDTO> getCurrentUserTransactionList(
        Long walletId,
        TransactionStatus status,
        ZonedDateTime start,
        ZonedDateTime end,
        Pageable pageable
    ) {
        return transactionRepository.getCurrentUserTransactionList(walletId, status, start, end, pageable).map(transactionMapper::toDto);
    }

    @Override
    public Page<TransactionDTO> getCurrentUserLatestTransactionList(Long walletId, TransactionStatus status, int size) {
        PageRequest pageable = PageRequest.of(0, size, Sort.by("createdDate").descending());
        return transactionRepository.getCurrentUserLatestTransactionList(walletId, status, pageable).map(transactionMapper::toDto);
    }

    @Override
    public List<TransactionMonthlySummaryProjection> getMonthlySummaryFromDateRange(
        WalletDTO wallet,
        ZonedDateTime start,
        ZonedDateTime end
    ) {
        return transactionRepository.getMonthlySummaryFromDateRange(wallet.getId(), start, end, wallet.getCurrency().getTimeZone());
    }
}

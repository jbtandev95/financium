package com.jbtan.financium.service.impl;

import com.jbtan.financium.domain.SpendeeTransactionMigration;
import com.jbtan.financium.repository.SpendeeTransactionMigrationRepository;
import com.jbtan.financium.service.SpendeeTransactionMigrationService;
import com.jbtan.financium.service.dto.SpendeeTransactionMigrationDTO;
import com.jbtan.financium.service.mapper.SpendeeTransactionMigrationMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link SpendeeTransactionMigration}.
 */
@Service
@Transactional
public class SpendeeTransactionMigrationServiceImpl implements SpendeeTransactionMigrationService {

    private final Logger log = LoggerFactory.getLogger(SpendeeTransactionMigrationServiceImpl.class);

    private final SpendeeTransactionMigrationRepository spendeeTransactionMigrationRepository;

    private final SpendeeTransactionMigrationMapper spendeeTransactionMigrationMapper;

    public SpendeeTransactionMigrationServiceImpl(
        SpendeeTransactionMigrationRepository spendeeTransactionMigrationRepository,
        SpendeeTransactionMigrationMapper spendeeTransactionMigrationMapper
    ) {
        this.spendeeTransactionMigrationRepository = spendeeTransactionMigrationRepository;
        this.spendeeTransactionMigrationMapper = spendeeTransactionMigrationMapper;
    }

    @Override
    public SpendeeTransactionMigrationDTO save(SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO) {
        log.debug("Request to save SpendeeTransactionMigration : {}", spendeeTransactionMigrationDTO);
        SpendeeTransactionMigration spendeeTransactionMigration = spendeeTransactionMigrationMapper.toEntity(
            spendeeTransactionMigrationDTO
        );
        spendeeTransactionMigration = spendeeTransactionMigrationRepository.save(spendeeTransactionMigration);
        return spendeeTransactionMigrationMapper.toDto(spendeeTransactionMigration);
    }

    @Override
    public SpendeeTransactionMigrationDTO update(SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO) {
        log.debug("Request to update SpendeeTransactionMigration : {}", spendeeTransactionMigrationDTO);
        SpendeeTransactionMigration spendeeTransactionMigration = spendeeTransactionMigrationMapper.toEntity(
            spendeeTransactionMigrationDTO
        );
        spendeeTransactionMigration = spendeeTransactionMigrationRepository.save(spendeeTransactionMigration);
        return spendeeTransactionMigrationMapper.toDto(spendeeTransactionMigration);
    }

    @Override
    public Optional<SpendeeTransactionMigrationDTO> partialUpdate(SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO) {
        log.debug("Request to partially update SpendeeTransactionMigration : {}", spendeeTransactionMigrationDTO);

        return spendeeTransactionMigrationRepository
            .findById(spendeeTransactionMigrationDTO.getId())
            .map(existingSpendeeTransactionMigration -> {
                spendeeTransactionMigrationMapper.partialUpdate(existingSpendeeTransactionMigration, spendeeTransactionMigrationDTO);

                return existingSpendeeTransactionMigration;
            })
            .map(spendeeTransactionMigrationRepository::save)
            .map(spendeeTransactionMigrationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<SpendeeTransactionMigrationDTO> findAll() {
        log.debug("Request to get all SpendeeTransactionMigrations");
        return spendeeTransactionMigrationRepository
            .findAll()
            .stream()
            .map(spendeeTransactionMigrationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<SpendeeTransactionMigrationDTO> findOne(Long id) {
        log.debug("Request to get SpendeeTransactionMigration : {}", id);
        return spendeeTransactionMigrationRepository.findById(id).map(spendeeTransactionMigrationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete SpendeeTransactionMigration : {}", id);
        spendeeTransactionMigrationRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> findDistinctCategoryByUserId(Long userId) {
        return spendeeTransactionMigrationRepository.findDistinctCategoryByUserId(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<SpendeeTransactionMigration> findAllByCategoryName(String categoryName) {
        return spendeeTransactionMigrationRepository.findAllByCategoryName(categoryName);
    }
}

package com.jbtan.financium.service.impl;

import com.jbtan.financium.domain.TransactionTypeMigration;
import com.jbtan.financium.repository.TransactionTypeMigrationRepository;
import com.jbtan.financium.service.TransactionTypeMigrationService;
import com.jbtan.financium.service.dto.TransactionTypeMigrationDTO;
import com.jbtan.financium.service.mapper.TransactionTypeMigrationMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link TransactionTypeMigration}.
 */
@Service
@Transactional(readOnly = true)
public class TransactionTypeMigrationServiceImpl implements TransactionTypeMigrationService {

    private final Logger log = LoggerFactory.getLogger(TransactionTypeMigrationServiceImpl.class);

    private final TransactionTypeMigrationRepository transactionTypeMigrationRepository;

    private final TransactionTypeMigrationMapper transactionTypeMigrationMapper;

    public TransactionTypeMigrationServiceImpl(
        TransactionTypeMigrationRepository transactionTypeMigrationRepository,
        TransactionTypeMigrationMapper transactionTypeMigrationMapper
    ) {
        this.transactionTypeMigrationRepository = transactionTypeMigrationRepository;
        this.transactionTypeMigrationMapper = transactionTypeMigrationMapper;
    }

    @Override
    @Transactional(readOnly = false)
    public TransactionTypeMigrationDTO save(TransactionTypeMigrationDTO transactionTypeMigrationDTO) {
        log.debug("Request to save TransactionTypeMigration : {}", transactionTypeMigrationDTO);
        TransactionTypeMigration transactionTypeMigration = transactionTypeMigrationMapper.toEntity(transactionTypeMigrationDTO);
        transactionTypeMigration = transactionTypeMigrationRepository.save(transactionTypeMigration);
        return transactionTypeMigrationMapper.toDto(transactionTypeMigration);
    }

    @Override
    @Transactional(readOnly = false)
    public TransactionTypeMigrationDTO update(TransactionTypeMigrationDTO transactionTypeMigrationDTO) {
        log.debug("Request to update TransactionTypeMigration : {}", transactionTypeMigrationDTO);
        TransactionTypeMigration transactionTypeMigration = transactionTypeMigrationMapper.toEntity(transactionTypeMigrationDTO);
        transactionTypeMigration = transactionTypeMigrationRepository.save(transactionTypeMigration);
        return transactionTypeMigrationMapper.toDto(transactionTypeMigration);
    }

    @Override
    @Transactional(readOnly = false)
    public Optional<TransactionTypeMigrationDTO> partialUpdate(TransactionTypeMigrationDTO transactionTypeMigrationDTO) {
        log.debug("Request to partially update TransactionTypeMigration : {}", transactionTypeMigrationDTO);

        return transactionTypeMigrationRepository
            .findById(transactionTypeMigrationDTO.getId())
            .map(existingTransactionTypeMigration -> {
                transactionTypeMigrationMapper.partialUpdate(existingTransactionTypeMigration, transactionTypeMigrationDTO);

                return existingTransactionTypeMigration;
            })
            .map(transactionTypeMigrationRepository::save)
            .map(transactionTypeMigrationMapper::toDto);
    }

    @Override
    public List<TransactionTypeMigrationDTO> findAll() {
        log.debug("Request to get all TransactionTypeMigrations");
        return transactionTypeMigrationRepository
            .findAll()
            .stream()
            .map(transactionTypeMigrationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Optional<TransactionTypeMigrationDTO> findOne(Long id) {
        log.debug("Request to get TransactionTypeMigration : {}", id);
        return transactionTypeMigrationRepository.findById(id).map(transactionTypeMigrationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Long id) {
        log.debug("Request to delete TransactionTypeMigration : {}", id);
        transactionTypeMigrationRepository.deleteById(id);
    }
}

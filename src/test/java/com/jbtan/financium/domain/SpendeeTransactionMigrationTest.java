package com.jbtan.financium.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.jbtan.financium.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SpendeeTransactionMigrationTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpendeeTransactionMigration.class);
        SpendeeTransactionMigration spendeeTransactionMigration1 = new SpendeeTransactionMigration();
        spendeeTransactionMigration1.setId(1L);
        SpendeeTransactionMigration spendeeTransactionMigration2 = new SpendeeTransactionMigration();
        spendeeTransactionMigration2.setId(spendeeTransactionMigration1.getId());
        assertThat(spendeeTransactionMigration1).isEqualTo(spendeeTransactionMigration2);
        spendeeTransactionMigration2.setId(2L);
        assertThat(spendeeTransactionMigration1).isNotEqualTo(spendeeTransactionMigration2);
        spendeeTransactionMigration1.setId(null);
        assertThat(spendeeTransactionMigration1).isNotEqualTo(spendeeTransactionMigration2);
    }
}

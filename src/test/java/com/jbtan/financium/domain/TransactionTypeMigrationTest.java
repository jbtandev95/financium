package com.jbtan.financium.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.jbtan.financium.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TransactionTypeMigrationTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TransactionTypeMigration.class);
        TransactionTypeMigration transactionTypeMigration1 = new TransactionTypeMigration();
        transactionTypeMigration1.setId(1L);
        TransactionTypeMigration transactionTypeMigration2 = new TransactionTypeMigration();
        transactionTypeMigration2.setId(transactionTypeMigration1.getId());
        assertThat(transactionTypeMigration1).isEqualTo(transactionTypeMigration2);
        transactionTypeMigration2.setId(2L);
        assertThat(transactionTypeMigration1).isNotEqualTo(transactionTypeMigration2);
        transactionTypeMigration1.setId(null);
        assertThat(transactionTypeMigration1).isNotEqualTo(transactionTypeMigration2);
    }
}

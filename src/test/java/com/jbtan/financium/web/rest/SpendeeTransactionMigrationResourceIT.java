package com.jbtan.financium.web.rest;

import static com.jbtan.financium.web.rest.TestUtil.sameInstant;
import static com.jbtan.financium.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.jbtan.financium.IntegrationTest;
import com.jbtan.financium.domain.SpendeeTransactionMigration;
import com.jbtan.financium.domain.enumeration.CurrencyCode;
import com.jbtan.financium.repository.SpendeeTransactionMigrationRepository;
import com.jbtan.financium.service.dto.SpendeeTransactionMigrationDTO;
import com.jbtan.financium.service.mapper.SpendeeTransactionMigrationMapper;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SpendeeTransactionMigrationResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SpendeeTransactionMigrationResourceIT {

    private static final String DEFAULT_CATEGORY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CATEGORY_NAME = "BBBBBBBBBB";

    private static final CurrencyCode DEFAULT_CURRENCY = CurrencyCode.MYR;
    private static final CurrencyCode UPDATED_CURRENCY = CurrencyCode.SGD;

    private static final BigDecimal DEFAULT_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT = new BigDecimal(2);

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TRANSACTION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TRANSACTION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/spendee-transaction-migrations";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SpendeeTransactionMigrationRepository spendeeTransactionMigrationRepository;

    @Autowired
    private SpendeeTransactionMigrationMapper spendeeTransactionMigrationMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSpendeeTransactionMigrationMockMvc;

    private SpendeeTransactionMigration spendeeTransactionMigration;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpendeeTransactionMigration createEntity(EntityManager em) {
        SpendeeTransactionMigration spendeeTransactionMigration = new SpendeeTransactionMigration()
            .categoryName(DEFAULT_CATEGORY_NAME)
            .currency(DEFAULT_CURRENCY)
            .amount(DEFAULT_AMOUNT)
            .note(DEFAULT_NOTE)
            .transactionDate(DEFAULT_TRANSACTION_DATE);
        return spendeeTransactionMigration;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SpendeeTransactionMigration createUpdatedEntity(EntityManager em) {
        SpendeeTransactionMigration spendeeTransactionMigration = new SpendeeTransactionMigration()
            .categoryName(UPDATED_CATEGORY_NAME)
            .currency(UPDATED_CURRENCY)
            .amount(UPDATED_AMOUNT)
            .note(UPDATED_NOTE)
            .transactionDate(UPDATED_TRANSACTION_DATE);
        return spendeeTransactionMigration;
    }

    @BeforeEach
    public void initTest() {
        spendeeTransactionMigration = createEntity(em);
    }

    @Test
    @Transactional
    void createSpendeeTransactionMigration() throws Exception {
        int databaseSizeBeforeCreate = spendeeTransactionMigrationRepository.findAll().size();
        // Create the SpendeeTransactionMigration
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            spendeeTransactionMigration
        );
        restSpendeeTransactionMigrationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the SpendeeTransactionMigration in the database
        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeCreate + 1);
        SpendeeTransactionMigration testSpendeeTransactionMigration = spendeeTransactionMigrationList.get(
            spendeeTransactionMigrationList.size() - 1
        );
        assertThat(testSpendeeTransactionMigration.getCategoryName()).isEqualTo(DEFAULT_CATEGORY_NAME);
        assertThat(testSpendeeTransactionMigration.getCurrency()).isEqualTo(DEFAULT_CURRENCY);
        assertThat(testSpendeeTransactionMigration.getAmount()).isEqualByComparingTo(DEFAULT_AMOUNT);
        assertThat(testSpendeeTransactionMigration.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testSpendeeTransactionMigration.getTransactionDate()).isEqualTo(DEFAULT_TRANSACTION_DATE);
    }

    @Test
    @Transactional
    void createSpendeeTransactionMigrationWithExistingId() throws Exception {
        // Create the SpendeeTransactionMigration with an existing ID
        spendeeTransactionMigration.setId(1L);
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            spendeeTransactionMigration
        );

        int databaseSizeBeforeCreate = spendeeTransactionMigrationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSpendeeTransactionMigrationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the SpendeeTransactionMigration in the database
        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCategoryNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = spendeeTransactionMigrationRepository.findAll().size();
        // set the field null
        spendeeTransactionMigration.setCategoryName(null);

        // Create the SpendeeTransactionMigration, which fails.
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            spendeeTransactionMigration
        );

        restSpendeeTransactionMigrationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCurrencyIsRequired() throws Exception {
        int databaseSizeBeforeTest = spendeeTransactionMigrationRepository.findAll().size();
        // set the field null
        spendeeTransactionMigration.setCurrency(null);

        // Create the SpendeeTransactionMigration, which fails.
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            spendeeTransactionMigration
        );

        restSpendeeTransactionMigrationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = spendeeTransactionMigrationRepository.findAll().size();
        // set the field null
        spendeeTransactionMigration.setAmount(null);

        // Create the SpendeeTransactionMigration, which fails.
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            spendeeTransactionMigration
        );

        restSpendeeTransactionMigrationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNoteIsRequired() throws Exception {
        int databaseSizeBeforeTest = spendeeTransactionMigrationRepository.findAll().size();
        // set the field null
        spendeeTransactionMigration.setNote(null);

        // Create the SpendeeTransactionMigration, which fails.
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            spendeeTransactionMigration
        );

        restSpendeeTransactionMigrationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTransactionDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = spendeeTransactionMigrationRepository.findAll().size();
        // set the field null
        spendeeTransactionMigration.setTransactionDate(null);

        // Create the SpendeeTransactionMigration, which fails.
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            spendeeTransactionMigration
        );

        restSpendeeTransactionMigrationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllSpendeeTransactionMigrations() throws Exception {
        // Initialize the database
        spendeeTransactionMigrationRepository.saveAndFlush(spendeeTransactionMigration);

        // Get all the spendeeTransactionMigrationList
        restSpendeeTransactionMigrationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(spendeeTransactionMigration.getId().intValue())))
            .andExpect(jsonPath("$.[*].categoryName").value(hasItem(DEFAULT_CATEGORY_NAME)))
            .andExpect(jsonPath("$.[*].currency").value(hasItem(DEFAULT_CURRENCY.toString())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(sameNumber(DEFAULT_AMOUNT))))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE)))
            .andExpect(jsonPath("$.[*].transactionDate").value(hasItem(sameInstant(DEFAULT_TRANSACTION_DATE))));
    }

    @Test
    @Transactional
    void getSpendeeTransactionMigration() throws Exception {
        // Initialize the database
        spendeeTransactionMigrationRepository.saveAndFlush(spendeeTransactionMigration);

        // Get the spendeeTransactionMigration
        restSpendeeTransactionMigrationMockMvc
            .perform(get(ENTITY_API_URL_ID, spendeeTransactionMigration.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(spendeeTransactionMigration.getId().intValue()))
            .andExpect(jsonPath("$.categoryName").value(DEFAULT_CATEGORY_NAME))
            .andExpect(jsonPath("$.currency").value(DEFAULT_CURRENCY.toString()))
            .andExpect(jsonPath("$.amount").value(sameNumber(DEFAULT_AMOUNT)))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE))
            .andExpect(jsonPath("$.transactionDate").value(sameInstant(DEFAULT_TRANSACTION_DATE)));
    }

    @Test
    @Transactional
    void getNonExistingSpendeeTransactionMigration() throws Exception {
        // Get the spendeeTransactionMigration
        restSpendeeTransactionMigrationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingSpendeeTransactionMigration() throws Exception {
        // Initialize the database
        spendeeTransactionMigrationRepository.saveAndFlush(spendeeTransactionMigration);

        int databaseSizeBeforeUpdate = spendeeTransactionMigrationRepository.findAll().size();

        // Update the spendeeTransactionMigration
        SpendeeTransactionMigration updatedSpendeeTransactionMigration = spendeeTransactionMigrationRepository
            .findById(spendeeTransactionMigration.getId())
            .get();
        // Disconnect from session so that the updates on updatedSpendeeTransactionMigration are not directly saved in db
        em.detach(updatedSpendeeTransactionMigration);
        updatedSpendeeTransactionMigration
            .categoryName(UPDATED_CATEGORY_NAME)
            .currency(UPDATED_CURRENCY)
            .amount(UPDATED_AMOUNT)
            .note(UPDATED_NOTE)
            .transactionDate(UPDATED_TRANSACTION_DATE);
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            updatedSpendeeTransactionMigration
        );

        restSpendeeTransactionMigrationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, spendeeTransactionMigrationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isOk());

        // Validate the SpendeeTransactionMigration in the database
        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeUpdate);
        SpendeeTransactionMigration testSpendeeTransactionMigration = spendeeTransactionMigrationList.get(
            spendeeTransactionMigrationList.size() - 1
        );
        assertThat(testSpendeeTransactionMigration.getCategoryName()).isEqualTo(UPDATED_CATEGORY_NAME);
        assertThat(testSpendeeTransactionMigration.getCurrency()).isEqualTo(UPDATED_CURRENCY);
        assertThat(testSpendeeTransactionMigration.getAmount()).isEqualByComparingTo(UPDATED_AMOUNT);
        assertThat(testSpendeeTransactionMigration.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testSpendeeTransactionMigration.getTransactionDate()).isEqualTo(UPDATED_TRANSACTION_DATE);
    }

    @Test
    @Transactional
    void putNonExistingSpendeeTransactionMigration() throws Exception {
        int databaseSizeBeforeUpdate = spendeeTransactionMigrationRepository.findAll().size();
        spendeeTransactionMigration.setId(count.incrementAndGet());

        // Create the SpendeeTransactionMigration
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            spendeeTransactionMigration
        );

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpendeeTransactionMigrationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, spendeeTransactionMigrationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the SpendeeTransactionMigration in the database
        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSpendeeTransactionMigration() throws Exception {
        int databaseSizeBeforeUpdate = spendeeTransactionMigrationRepository.findAll().size();
        spendeeTransactionMigration.setId(count.incrementAndGet());

        // Create the SpendeeTransactionMigration
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            spendeeTransactionMigration
        );

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSpendeeTransactionMigrationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the SpendeeTransactionMigration in the database
        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSpendeeTransactionMigration() throws Exception {
        int databaseSizeBeforeUpdate = spendeeTransactionMigrationRepository.findAll().size();
        spendeeTransactionMigration.setId(count.incrementAndGet());

        // Create the SpendeeTransactionMigration
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            spendeeTransactionMigration
        );

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSpendeeTransactionMigrationMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the SpendeeTransactionMigration in the database
        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSpendeeTransactionMigrationWithPatch() throws Exception {
        // Initialize the database
        spendeeTransactionMigrationRepository.saveAndFlush(spendeeTransactionMigration);

        int databaseSizeBeforeUpdate = spendeeTransactionMigrationRepository.findAll().size();

        // Update the spendeeTransactionMigration using partial update
        SpendeeTransactionMigration partialUpdatedSpendeeTransactionMigration = new SpendeeTransactionMigration();
        partialUpdatedSpendeeTransactionMigration.setId(spendeeTransactionMigration.getId());

        partialUpdatedSpendeeTransactionMigration.note(UPDATED_NOTE);

        restSpendeeTransactionMigrationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSpendeeTransactionMigration.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSpendeeTransactionMigration))
            )
            .andExpect(status().isOk());

        // Validate the SpendeeTransactionMigration in the database
        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeUpdate);
        SpendeeTransactionMigration testSpendeeTransactionMigration = spendeeTransactionMigrationList.get(
            spendeeTransactionMigrationList.size() - 1
        );
        assertThat(testSpendeeTransactionMigration.getCategoryName()).isEqualTo(DEFAULT_CATEGORY_NAME);
        assertThat(testSpendeeTransactionMigration.getCurrency()).isEqualTo(DEFAULT_CURRENCY);
        assertThat(testSpendeeTransactionMigration.getAmount()).isEqualByComparingTo(DEFAULT_AMOUNT);
        assertThat(testSpendeeTransactionMigration.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testSpendeeTransactionMigration.getTransactionDate()).isEqualTo(DEFAULT_TRANSACTION_DATE);
    }

    @Test
    @Transactional
    void fullUpdateSpendeeTransactionMigrationWithPatch() throws Exception {
        // Initialize the database
        spendeeTransactionMigrationRepository.saveAndFlush(spendeeTransactionMigration);

        int databaseSizeBeforeUpdate = spendeeTransactionMigrationRepository.findAll().size();

        // Update the spendeeTransactionMigration using partial update
        SpendeeTransactionMigration partialUpdatedSpendeeTransactionMigration = new SpendeeTransactionMigration();
        partialUpdatedSpendeeTransactionMigration.setId(spendeeTransactionMigration.getId());

        partialUpdatedSpendeeTransactionMigration
            .categoryName(UPDATED_CATEGORY_NAME)
            .currency(UPDATED_CURRENCY)
            .amount(UPDATED_AMOUNT)
            .note(UPDATED_NOTE)
            .transactionDate(UPDATED_TRANSACTION_DATE);

        restSpendeeTransactionMigrationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSpendeeTransactionMigration.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSpendeeTransactionMigration))
            )
            .andExpect(status().isOk());

        // Validate the SpendeeTransactionMigration in the database
        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeUpdate);
        SpendeeTransactionMigration testSpendeeTransactionMigration = spendeeTransactionMigrationList.get(
            spendeeTransactionMigrationList.size() - 1
        );
        assertThat(testSpendeeTransactionMigration.getCategoryName()).isEqualTo(UPDATED_CATEGORY_NAME);
        assertThat(testSpendeeTransactionMigration.getCurrency()).isEqualTo(UPDATED_CURRENCY);
        assertThat(testSpendeeTransactionMigration.getAmount()).isEqualByComparingTo(UPDATED_AMOUNT);
        assertThat(testSpendeeTransactionMigration.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testSpendeeTransactionMigration.getTransactionDate()).isEqualTo(UPDATED_TRANSACTION_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingSpendeeTransactionMigration() throws Exception {
        int databaseSizeBeforeUpdate = spendeeTransactionMigrationRepository.findAll().size();
        spendeeTransactionMigration.setId(count.incrementAndGet());

        // Create the SpendeeTransactionMigration
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            spendeeTransactionMigration
        );

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSpendeeTransactionMigrationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, spendeeTransactionMigrationDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the SpendeeTransactionMigration in the database
        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSpendeeTransactionMigration() throws Exception {
        int databaseSizeBeforeUpdate = spendeeTransactionMigrationRepository.findAll().size();
        spendeeTransactionMigration.setId(count.incrementAndGet());

        // Create the SpendeeTransactionMigration
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            spendeeTransactionMigration
        );

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSpendeeTransactionMigrationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the SpendeeTransactionMigration in the database
        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSpendeeTransactionMigration() throws Exception {
        int databaseSizeBeforeUpdate = spendeeTransactionMigrationRepository.findAll().size();
        spendeeTransactionMigration.setId(count.incrementAndGet());

        // Create the SpendeeTransactionMigration
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO = spendeeTransactionMigrationMapper.toDto(
            spendeeTransactionMigration
        );

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSpendeeTransactionMigrationMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(spendeeTransactionMigrationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the SpendeeTransactionMigration in the database
        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSpendeeTransactionMigration() throws Exception {
        // Initialize the database
        spendeeTransactionMigrationRepository.saveAndFlush(spendeeTransactionMigration);

        int databaseSizeBeforeDelete = spendeeTransactionMigrationRepository.findAll().size();

        // Delete the spendeeTransactionMigration
        restSpendeeTransactionMigrationMockMvc
            .perform(delete(ENTITY_API_URL_ID, spendeeTransactionMigration.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SpendeeTransactionMigration> spendeeTransactionMigrationList = spendeeTransactionMigrationRepository.findAll();
        assertThat(spendeeTransactionMigrationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

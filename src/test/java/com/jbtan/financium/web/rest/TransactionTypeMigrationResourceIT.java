package com.jbtan.financium.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.jbtan.financium.IntegrationTest;
import com.jbtan.financium.domain.TransactionTypeMigration;
import com.jbtan.financium.repository.TransactionTypeMigrationRepository;
import com.jbtan.financium.service.dto.TransactionTypeMigrationDTO;
import com.jbtan.financium.service.mapper.TransactionTypeMigrationMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link TransactionTypeMigrationResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class TransactionTypeMigrationResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/transaction-type-migrations";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TransactionTypeMigrationRepository transactionTypeMigrationRepository;

    @Autowired
    private TransactionTypeMigrationMapper transactionTypeMigrationMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTransactionTypeMigrationMockMvc;

    private TransactionTypeMigration transactionTypeMigration;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TransactionTypeMigration createEntity(EntityManager em) {
        TransactionTypeMigration transactionTypeMigration = new TransactionTypeMigration().name(DEFAULT_NAME);
        return transactionTypeMigration;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TransactionTypeMigration createUpdatedEntity(EntityManager em) {
        TransactionTypeMigration transactionTypeMigration = new TransactionTypeMigration().name(UPDATED_NAME);
        return transactionTypeMigration;
    }

    @BeforeEach
    public void initTest() {
        transactionTypeMigration = createEntity(em);
    }

    @Test
    @Transactional
    void createTransactionTypeMigration() throws Exception {
        int databaseSizeBeforeCreate = transactionTypeMigrationRepository.findAll().size();
        // Create the TransactionTypeMigration
        TransactionTypeMigrationDTO transactionTypeMigrationDTO = transactionTypeMigrationMapper.toDto(transactionTypeMigration);
        restTransactionTypeMigrationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactionTypeMigrationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the TransactionTypeMigration in the database
        List<TransactionTypeMigration> transactionTypeMigrationList = transactionTypeMigrationRepository.findAll();
        assertThat(transactionTypeMigrationList).hasSize(databaseSizeBeforeCreate + 1);
        TransactionTypeMigration testTransactionTypeMigration = transactionTypeMigrationList.get(transactionTypeMigrationList.size() - 1);
        assertThat(testTransactionTypeMigration.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void createTransactionTypeMigrationWithExistingId() throws Exception {
        // Create the TransactionTypeMigration with an existing ID
        transactionTypeMigration.setId(1L);
        TransactionTypeMigrationDTO transactionTypeMigrationDTO = transactionTypeMigrationMapper.toDto(transactionTypeMigration);

        int databaseSizeBeforeCreate = transactionTypeMigrationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransactionTypeMigrationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactionTypeMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TransactionTypeMigration in the database
        List<TransactionTypeMigration> transactionTypeMigrationList = transactionTypeMigrationRepository.findAll();
        assertThat(transactionTypeMigrationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = transactionTypeMigrationRepository.findAll().size();
        // set the field null
        transactionTypeMigration.setName(null);

        // Create the TransactionTypeMigration, which fails.
        TransactionTypeMigrationDTO transactionTypeMigrationDTO = transactionTypeMigrationMapper.toDto(transactionTypeMigration);

        restTransactionTypeMigrationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactionTypeMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        List<TransactionTypeMigration> transactionTypeMigrationList = transactionTypeMigrationRepository.findAll();
        assertThat(transactionTypeMigrationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllTransactionTypeMigrations() throws Exception {
        // Initialize the database
        transactionTypeMigrationRepository.saveAndFlush(transactionTypeMigration);

        // Get all the transactionTypeMigrationList
        restTransactionTypeMigrationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transactionTypeMigration.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getTransactionTypeMigration() throws Exception {
        // Initialize the database
        transactionTypeMigrationRepository.saveAndFlush(transactionTypeMigration);

        // Get the transactionTypeMigration
        restTransactionTypeMigrationMockMvc
            .perform(get(ENTITY_API_URL_ID, transactionTypeMigration.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(transactionTypeMigration.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getNonExistingTransactionTypeMigration() throws Exception {
        // Get the transactionTypeMigration
        restTransactionTypeMigrationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingTransactionTypeMigration() throws Exception {
        // Initialize the database
        transactionTypeMigrationRepository.saveAndFlush(transactionTypeMigration);

        int databaseSizeBeforeUpdate = transactionTypeMigrationRepository.findAll().size();

        // Update the transactionTypeMigration
        TransactionTypeMigration updatedTransactionTypeMigration = transactionTypeMigrationRepository
            .findById(transactionTypeMigration.getId())
            .get();
        // Disconnect from session so that the updates on updatedTransactionTypeMigration are not directly saved in db
        em.detach(updatedTransactionTypeMigration);
        updatedTransactionTypeMigration.name(UPDATED_NAME);
        TransactionTypeMigrationDTO transactionTypeMigrationDTO = transactionTypeMigrationMapper.toDto(updatedTransactionTypeMigration);

        restTransactionTypeMigrationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, transactionTypeMigrationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactionTypeMigrationDTO))
            )
            .andExpect(status().isOk());

        // Validate the TransactionTypeMigration in the database
        List<TransactionTypeMigration> transactionTypeMigrationList = transactionTypeMigrationRepository.findAll();
        assertThat(transactionTypeMigrationList).hasSize(databaseSizeBeforeUpdate);
        TransactionTypeMigration testTransactionTypeMigration = transactionTypeMigrationList.get(transactionTypeMigrationList.size() - 1);
        assertThat(testTransactionTypeMigration.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void putNonExistingTransactionTypeMigration() throws Exception {
        int databaseSizeBeforeUpdate = transactionTypeMigrationRepository.findAll().size();
        transactionTypeMigration.setId(count.incrementAndGet());

        // Create the TransactionTypeMigration
        TransactionTypeMigrationDTO transactionTypeMigrationDTO = transactionTypeMigrationMapper.toDto(transactionTypeMigration);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTransactionTypeMigrationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, transactionTypeMigrationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactionTypeMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TransactionTypeMigration in the database
        List<TransactionTypeMigration> transactionTypeMigrationList = transactionTypeMigrationRepository.findAll();
        assertThat(transactionTypeMigrationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchTransactionTypeMigration() throws Exception {
        int databaseSizeBeforeUpdate = transactionTypeMigrationRepository.findAll().size();
        transactionTypeMigration.setId(count.incrementAndGet());

        // Create the TransactionTypeMigration
        TransactionTypeMigrationDTO transactionTypeMigrationDTO = transactionTypeMigrationMapper.toDto(transactionTypeMigration);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTransactionTypeMigrationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactionTypeMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TransactionTypeMigration in the database
        List<TransactionTypeMigration> transactionTypeMigrationList = transactionTypeMigrationRepository.findAll();
        assertThat(transactionTypeMigrationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamTransactionTypeMigration() throws Exception {
        int databaseSizeBeforeUpdate = transactionTypeMigrationRepository.findAll().size();
        transactionTypeMigration.setId(count.incrementAndGet());

        // Create the TransactionTypeMigration
        TransactionTypeMigrationDTO transactionTypeMigrationDTO = transactionTypeMigrationMapper.toDto(transactionTypeMigration);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTransactionTypeMigrationMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactionTypeMigrationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the TransactionTypeMigration in the database
        List<TransactionTypeMigration> transactionTypeMigrationList = transactionTypeMigrationRepository.findAll();
        assertThat(transactionTypeMigrationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateTransactionTypeMigrationWithPatch() throws Exception {
        // Initialize the database
        transactionTypeMigrationRepository.saveAndFlush(transactionTypeMigration);

        int databaseSizeBeforeUpdate = transactionTypeMigrationRepository.findAll().size();

        // Update the transactionTypeMigration using partial update
        TransactionTypeMigration partialUpdatedTransactionTypeMigration = new TransactionTypeMigration();
        partialUpdatedTransactionTypeMigration.setId(transactionTypeMigration.getId());

        restTransactionTypeMigrationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTransactionTypeMigration.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTransactionTypeMigration))
            )
            .andExpect(status().isOk());

        // Validate the TransactionTypeMigration in the database
        List<TransactionTypeMigration> transactionTypeMigrationList = transactionTypeMigrationRepository.findAll();
        assertThat(transactionTypeMigrationList).hasSize(databaseSizeBeforeUpdate);
        TransactionTypeMigration testTransactionTypeMigration = transactionTypeMigrationList.get(transactionTypeMigrationList.size() - 1);
        assertThat(testTransactionTypeMigration.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void fullUpdateTransactionTypeMigrationWithPatch() throws Exception {
        // Initialize the database
        transactionTypeMigrationRepository.saveAndFlush(transactionTypeMigration);

        int databaseSizeBeforeUpdate = transactionTypeMigrationRepository.findAll().size();

        // Update the transactionTypeMigration using partial update
        TransactionTypeMigration partialUpdatedTransactionTypeMigration = new TransactionTypeMigration();
        partialUpdatedTransactionTypeMigration.setId(transactionTypeMigration.getId());

        partialUpdatedTransactionTypeMigration.name(UPDATED_NAME);

        restTransactionTypeMigrationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTransactionTypeMigration.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTransactionTypeMigration))
            )
            .andExpect(status().isOk());

        // Validate the TransactionTypeMigration in the database
        List<TransactionTypeMigration> transactionTypeMigrationList = transactionTypeMigrationRepository.findAll();
        assertThat(transactionTypeMigrationList).hasSize(databaseSizeBeforeUpdate);
        TransactionTypeMigration testTransactionTypeMigration = transactionTypeMigrationList.get(transactionTypeMigrationList.size() - 1);
        assertThat(testTransactionTypeMigration.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingTransactionTypeMigration() throws Exception {
        int databaseSizeBeforeUpdate = transactionTypeMigrationRepository.findAll().size();
        transactionTypeMigration.setId(count.incrementAndGet());

        // Create the TransactionTypeMigration
        TransactionTypeMigrationDTO transactionTypeMigrationDTO = transactionTypeMigrationMapper.toDto(transactionTypeMigration);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTransactionTypeMigrationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, transactionTypeMigrationDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(transactionTypeMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TransactionTypeMigration in the database
        List<TransactionTypeMigration> transactionTypeMigrationList = transactionTypeMigrationRepository.findAll();
        assertThat(transactionTypeMigrationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchTransactionTypeMigration() throws Exception {
        int databaseSizeBeforeUpdate = transactionTypeMigrationRepository.findAll().size();
        transactionTypeMigration.setId(count.incrementAndGet());

        // Create the TransactionTypeMigration
        TransactionTypeMigrationDTO transactionTypeMigrationDTO = transactionTypeMigrationMapper.toDto(transactionTypeMigration);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTransactionTypeMigrationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(transactionTypeMigrationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TransactionTypeMigration in the database
        List<TransactionTypeMigration> transactionTypeMigrationList = transactionTypeMigrationRepository.findAll();
        assertThat(transactionTypeMigrationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamTransactionTypeMigration() throws Exception {
        int databaseSizeBeforeUpdate = transactionTypeMigrationRepository.findAll().size();
        transactionTypeMigration.setId(count.incrementAndGet());

        // Create the TransactionTypeMigration
        TransactionTypeMigrationDTO transactionTypeMigrationDTO = transactionTypeMigrationMapper.toDto(transactionTypeMigration);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTransactionTypeMigrationMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(transactionTypeMigrationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the TransactionTypeMigration in the database
        List<TransactionTypeMigration> transactionTypeMigrationList = transactionTypeMigrationRepository.findAll();
        assertThat(transactionTypeMigrationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteTransactionTypeMigration() throws Exception {
        // Initialize the database
        transactionTypeMigrationRepository.saveAndFlush(transactionTypeMigration);

        int databaseSizeBeforeDelete = transactionTypeMigrationRepository.findAll().size();

        // Delete the transactionTypeMigration
        restTransactionTypeMigrationMockMvc
            .perform(delete(ENTITY_API_URL_ID, transactionTypeMigration.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TransactionTypeMigration> transactionTypeMigrationList = transactionTypeMigrationRepository.findAll();
        assertThat(transactionTypeMigrationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package com.jbtan.financium.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.jbtan.financium.domain.enumeration.CurrencyCode;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import org.junit.jupiter.api.Test;

public class DateTimeUtilTest {

    @Test
    void testGetZonedStartTime() {
        // Create a LocalDate object for testing
        LocalDate start = LocalDate.of(2023, 3, 1);

        CurrencyCode currency = CurrencyCode.MYR;

        // Call the utility function and verify the result
        ZonedDateTime zonedStartTime = DateTimeUtil.getZonedStartTime(start, currency);
        assertEquals(zonedStartTime, ZonedDateTime.of(2023, 3, 1, 0, 0, 0, 0, ZoneId.of("Asia/Kuala_Lumpur")));
    }

    @Test
    void testGetZonedStartTimeWithDifferentTimeZone() {
        // Create a LocalDate object for testing
        LocalDate start = LocalDate.of(2023, 3, 1);

        CurrencyCode currency = CurrencyCode.MYR;

        // Call the utility function and verify the result
        ZonedDateTime zonedStartTime = DateTimeUtil.getZonedStartTime(start, currency);
        assertEquals(zonedStartTime, ZonedDateTime.of(2023, 3, 1, 0, 0, 0, 0, ZoneId.of("Asia/Kuala_Lumpur")));
    }

    @Test
    void testGetZonedStartTimeWithNullStart() {
        // Create a CurrencyCode object with a mocked ZoneId
        CurrencyCode currency = CurrencyCode.MYR;

        // Call the utility function with a null start date and verify the exception is
        // thrown
        assertThrows(NullPointerException.class, () -> DateTimeUtil.getZonedStartTime(null, currency));
    }

    @Test
    void testGetZonedStartTimeWithNullCurrency() {
        // Create a LocalDate object for testing
        LocalDate start = LocalDate.of(2023, 3, 1);

        // Call the utility function with a null currency and verify the exception is
        // thrown
        assertThrows(NullPointerException.class, () -> DateTimeUtil.getZonedStartTime(start, null));
    }
}

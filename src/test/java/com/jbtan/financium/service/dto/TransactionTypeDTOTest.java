package com.jbtan.financium.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.jbtan.financium.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TransactionTypeDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TransactionTypeDTO.class);
        TransactionTypeDTO transactionTypeDTO1 = TransactionTypeDTO.builder().id(1L).build();
        TransactionTypeDTO transactionTypeDTO2 = TransactionTypeDTO.builder().build();
        assertThat(transactionTypeDTO1).isNotEqualTo(transactionTypeDTO2);
        transactionTypeDTO2.setId(transactionTypeDTO1.getId());
        assertThat(transactionTypeDTO1).isEqualTo(transactionTypeDTO2);
        transactionTypeDTO2.setId(2L);
        assertThat(transactionTypeDTO1).isNotEqualTo(transactionTypeDTO2);
        transactionTypeDTO1.setId(null);
        assertThat(transactionTypeDTO1).isNotEqualTo(transactionTypeDTO2);
    }
}

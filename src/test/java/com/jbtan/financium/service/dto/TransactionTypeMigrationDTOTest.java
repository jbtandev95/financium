package com.jbtan.financium.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.jbtan.financium.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TransactionTypeMigrationDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TransactionTypeMigrationDTO.class);
        TransactionTypeMigrationDTO transactionTypeMigrationDTO1 = new TransactionTypeMigrationDTO();
        transactionTypeMigrationDTO1.setId(1L);
        TransactionTypeMigrationDTO transactionTypeMigrationDTO2 = new TransactionTypeMigrationDTO();
        assertThat(transactionTypeMigrationDTO1).isNotEqualTo(transactionTypeMigrationDTO2);
        transactionTypeMigrationDTO2.setId(transactionTypeMigrationDTO1.getId());
        assertThat(transactionTypeMigrationDTO1).isEqualTo(transactionTypeMigrationDTO2);
        transactionTypeMigrationDTO2.setId(2L);
        assertThat(transactionTypeMigrationDTO1).isNotEqualTo(transactionTypeMigrationDTO2);
        transactionTypeMigrationDTO1.setId(null);
        assertThat(transactionTypeMigrationDTO1).isNotEqualTo(transactionTypeMigrationDTO2);
    }
}

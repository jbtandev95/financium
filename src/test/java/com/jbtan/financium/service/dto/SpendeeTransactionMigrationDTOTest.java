package com.jbtan.financium.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.jbtan.financium.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SpendeeTransactionMigrationDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpendeeTransactionMigrationDTO.class);
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO1 = new SpendeeTransactionMigrationDTO();
        spendeeTransactionMigrationDTO1.setId(1L);
        SpendeeTransactionMigrationDTO spendeeTransactionMigrationDTO2 = new SpendeeTransactionMigrationDTO();
        assertThat(spendeeTransactionMigrationDTO1).isNotEqualTo(spendeeTransactionMigrationDTO2);
        spendeeTransactionMigrationDTO2.setId(spendeeTransactionMigrationDTO1.getId());
        assertThat(spendeeTransactionMigrationDTO1).isEqualTo(spendeeTransactionMigrationDTO2);
        spendeeTransactionMigrationDTO2.setId(2L);
        assertThat(spendeeTransactionMigrationDTO1).isNotEqualTo(spendeeTransactionMigrationDTO2);
        spendeeTransactionMigrationDTO1.setId(null);
        assertThat(spendeeTransactionMigrationDTO1).isNotEqualTo(spendeeTransactionMigrationDTO2);
    }
}
